/**
 * riskservice.hpp
 * Defines the data types and Service for fixed income risk.
 *
 * @author Breman Thuraisingham
 */
#ifndef RISKSERVICE_HPP
#define RISKSERVICE_HPP

#include "soa.hpp"
#include "positionservice.hpp"
#include "products.hpp"
#include <map>
#include <string>
using namespace std;

/**
 * PV01 risk.
 * Type T is the product type.
 */
template <typename T>
class PV01 {

 public:

  PV01() = default;
  // ctor for a PV01 value
  PV01(const T &_product, double _pv01, long _quantity);

  // Get the product on this PV01 value
  const T &GetProduct() const;

  // Get the PV01 value
  double GetPV01() const;

  // Get the quantity that this risk value is associated with
  long GetQuantity() const;

  // set the PV01 value
  void SetPV01(double _pv01);

  // set the quantity
  void SetQuantity(long _quantity);

 private:
  T product;
  double pv01;
  long quantity;

};

template <typename T>
PV01<T>::PV01(const T &_product, double _pv01, long _quantity) :
    product(_product) {
  pv01 = _pv01;
  quantity = _quantity;
}

template <typename T>
const T &PV01<T>::GetProduct() const {
  return product;
}

template <typename T>
double PV01<T>::GetPV01() const {
  return pv01;
}

template <typename T>
long PV01<T>::GetQuantity() const {
  return quantity;
}

template <typename T>
void PV01<T>::SetPV01(double _pv01) {
  pv01 = _pv01;
}

template <typename T>
void PV01<T>::SetQuantity(long _quantity) {
  quantity = _quantity;
}

/**
 * A bucket sector to bucket a group of securities.
 * We can then aggregate bucketed risk to this bucket.
 * Type T is the product type.
 */
template <typename T>
class BucketedSector {

 public:

  BucketedSector() = default;
  // ctor for a bucket sector
  BucketedSector(const vector<T> &_products, string _name);

  // Get the products associated with this bucket
  const vector<T> &GetProducts() const;

  // Get the name of the bucket
  const string &GetName() const;

  // add products to the bucket
  void AddProducts(T _product);

 private:
  vector<T> products;
  string name;

};

template <typename T>
BucketedSector<T>::BucketedSector(const vector<T> &_products, string _name) :
    products(_products) {
  name = _name;
}

template <typename T>
const vector<T> &BucketedSector<T>::GetProducts() const {
  return products;
}

template <typename T>
const string &BucketedSector<T>::GetName() const {
  return name;
}

template <typename T>
void BucketedSector<T>::AddProducts(T _product) {
  products.push_back(_product);
}

// utility function to generate bucket
BucketedSector<Bond> GenerateBucket(string _cusip) {
  BucketedSector<Bond> sector;
  std::vector<Bond> vec;
  if (_cusip == "9128283H1") {
    Bond two_yr = GenerateBond("9128283H1");
    Bond three_yr = GenerateBond("9128283G3");
    vec.push_back(two_yr);
    vec.push_back(three_yr);
    sector = BucketedSector<Bond>(vec, "FrontEnd");
  }
  if (_cusip == "9128283G3") {
    Bond two_yr = GenerateBond("9128283H1");
    Bond three_yr = GenerateBond("9128283G3");
    vec.push_back(two_yr);
    vec.push_back(three_yr);
    sector = BucketedSector<Bond>(vec, "FrontEnd");
  }
  if (_cusip == "912828M80" || _cusip == "9128283J7" || _cusip == "9128283F5") {

    Bond five_yr = GenerateBond("912828M80");
    Bond seven_yr = GenerateBond("9128283J7");
    Bond ten_yr = GenerateBond("9128283F5");
    vec.push_back(five_yr);
    vec.push_back(seven_yr);

    sector = BucketedSector<Bond>(vec, "Belly");
  }
  if (_cusip == "912810RZ3") {

    Bond thirty_yr = GenerateBond("912810RZ3");

    vec.push_back(thirty_yr);
    sector = BucketedSector<Bond>(vec, "LongEnd");
  }
  return sector;

}

/**
 * Risk Service to vend out risk for a particular security and across a risk bucketed sector.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class RiskService : public Service<string, PV01<T> > {

 public:

  // Add a position that the service will risk
  virtual void AddPosition(Position<T> &_position) = 0;

  // Get the bucketed risk for the bucket sector
  virtual PV01<BucketedSector<T> > GetBucketedRisk(BucketedSector<T> &_sector) = 0;

};

class BondRiskService : public RiskService<Bond> {
 private:
  std::map<string, PV01<Bond>> riskMap = {};// map stores the risk data
  std::map<string, PV01<BucketedSector<Bond>>>
      bucketRiskMap = {};// stores the bucketed risk data, key on the bucket name
  std::vector<ServiceListener<PV01<Bond>> *> listeners;// servicelisteners contains the trade data
  BondRiskService() {};// private ctor corresponding to the singleton pattern we use in this class
 public:
  //singleton pattern - create a global access to the object
  static BondRiskService *getInstance();
  BondRiskService(const BondRiskService &_source) = delete;//invalid the copy ctor
  void operator=(const BondRiskService &_source) = delete;// invalid the assignment operator
  void OnMessage(PV01<Bond> &_risk) override {}
  PV01<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<PV01<Bond>> *_listener) override;
  const vector<ServiceListener<PV01<Bond>> *> &GetListeners() const override;
  void AddPosition(Position<Bond> &_position) override;// add position that the service will risk
  void UpdateBucketedRisk(BucketedSector<Bond> &_sector);
  PV01<BucketedSector<Bond>> GetBucketedRisk(BucketedSector<Bond> &_sector) override;
};

BondRiskService *BondRiskService::getInstance() {
  static BondRiskService instance;
  return &instance;
}

PV01<Bond> &BondRiskService::GetData(string _key) {
  return riskMap[_key];
}

void BondRiskService::AddListener(ServiceListener<PV01<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<PV01<Bond>> *> &BondRiskService::GetListeners() const {
  return listeners;
}

void BondRiskService::AddPosition(Position<Bond> &_position) {
  BucketedSector<Bond> sector = GenerateBucket(_position.GetProduct().GetProductId());
  if (riskMap.find(_position.GetProduct().GetProductId()) == riskMap.end()) {
    vector<string> id = {"9128283H1", "9128283G3", "912828M80", "9128283J7", "9128283F5", "912810RZ3"};
    vector<double> pv01 = {0.019851, 0.029309, 0.048643, 0.065843, 0.087939, 0.184698};
    for (int i = 0; i < id.size(); i++) {
      if (_position.GetProduct().GetProductId() == id[i]) {
        //std::cout<<"productId"<<id[i]<<endl;
        PV01<Bond> risk(_position.GetProduct(), pv01[i], _position.GetAggregatePosition());
        riskMap.insert(std::pair<string, PV01<Bond>>(id[i], risk));

        UpdateBucketedRisk(sector);
        //std::cout << "BondRiskService: AddPosition Method notifying the listeners...\n";
        for (auto &listener: listeners)
          listener->ProcessAdd(risk);// passing the PV01 data to the listeners
        break;
      }
    }
  } else {
    PV01<Bond> &risk = riskMap[_position.GetProduct().GetProductId()];
    risk.SetQuantity(_position.GetAggregatePosition());
    UpdateBucketedRisk(sector);
    //std::cout << "BondRiskService: AddPosition Method notifying the listeners...\n";
    for (auto &listener: listeners)
      listener->ProcessAdd(risk);// passing the updated PV01 data to the listeners
  }

}

void BondRiskService::UpdateBucketedRisk(BucketedSector<Bond> &_sector) {
  vector<Bond> bond_bucket = _sector.GetProducts();
  string productId;
  long total_position = 0;
  double total_risk = 0.0;
  for (Bond i: bond_bucket) {
    productId = i.GetProductId();
    if (riskMap.find(productId) != riskMap.end()) {
      total_position += riskMap.at(productId).GetQuantity();

      total_risk += riskMap.at(productId).GetPV01() * riskMap.at(productId).GetQuantity();

    }
  }
  double bucket_risk = 0.0;
  bucket_risk = total_risk / ( double ) total_position;
  PV01<BucketedSector<Bond>> res(_sector, bucket_risk, total_position);
  string bucketName = _sector.GetName();

  if (bucketRiskMap.find(bucketName) == bucketRiskMap.end()) {
    bucketRiskMap.insert(std::pair<string, PV01<BucketedSector<Bond>>>(bucketName, res));

  } else {
    bucketRiskMap[bucketName] = res;
  }
}

PV01<BucketedSector<Bond>> BondRiskService::GetBucketedRisk(BucketedSector<Bond> &_sector) {

  string bucketName = _sector.GetName();

  return bucketRiskMap[bucketName];

}

class BondRiskServiceListener : public ServiceListener<Position<Bond>> {
 private:
  BondRiskService *BRServ;
  BondRiskServiceListener() {
    BRServ = BondRiskService::getInstance();
  };

 public:
  static BondRiskServiceListener *getInstance();
  void ProcessAdd(Position<Bond> &_data) override;
  void ProcessRemove(Position<Bond> &_data) override;
  void ProcessUpdate(Position<Bond> &_data) override;
  BondRiskService *getService();
};

BondRiskServiceListener *BondRiskServiceListener::getInstance() {
  static BondRiskServiceListener instance;
  return &instance;
}

void BondRiskServiceListener::ProcessAdd(Position<Bond> &_data) {
  BRServ->AddPosition(_data);
}

void BondRiskServiceListener::ProcessUpdate(Position<Bond> &_data) {
  BRServ->AddPosition(_data);
}

void BondRiskServiceListener::ProcessRemove(Position<Bond> &_data) {
  //TBD
}

#endif
