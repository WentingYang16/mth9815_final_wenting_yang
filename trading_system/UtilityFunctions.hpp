//
// Created by Wenting Yang on 12/13/17.
//

#ifndef UTILITYFUNCTIONS_HPP
#define UTILITYFUNCTIONS_HPP

#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include "products.hpp"
//#include "riskservice.hpp"
#include <string>

using namespace std;

/*
 * Some global function that can be used when do the subscibe and publish with the txt file
 */

void Trim(std::string &str) {
  //trim all the spaces from beginning and end of string
  boost::trim(str);
  //trim all the spaces in the middle
  boost::trim_if(str, boost::is_any_of(" "));
}

string to_fractional(double price) {
  int integer = floor(price);
  int decimal = floor((price - integer) * 256.0);
  int xy_i = floor(decimal / 8.0);
  int z_i = decimal % 8;

  string integer_s = to_string(integer);
  string xy_s;
  if (xy_i < 10)
    xy_s = "0" + to_string(xy_i);
  else
    xy_s = to_string(xy_i);

  string z_s;
  if (z_i == 4)
    z_s = "+";
  else
    z_s = to_string(z_i);

  string stringPrice = integer_s + "-" + xy_s + z_s;
  return stringPrice;
}

double to_double(string price_s) {
  std::vector<std::string> vec;
  boost::split(vec, price_s, boost::is_any_of("-+"));
  int integer, xy, z;
  integer = std::stoi(vec[0]);
  //std::cout << "integer: "<<integer<<endl;
  if (vec.size() == 3) {
    xy = std::stoi(vec[1]);
    z = 4;
  }
  if (vec.size() == 2) {
    int tmp = std::stoi(vec[1]);
    xy = tmp / 10;
    z = tmp - 10 * xy;
  }
  //std::cout << "xy: "<<xy<<" z: "<<z<<endl;
  double price = ( double ) integer + ( double ) xy / 32.0 + ( double ) z / 256.0;
  return price;
}

Bond GenerateBond(string _cusip) {
  Bond bond;
  if (_cusip == "9128283H1")
    bond = Bond("9128283H1", CUSIP, "T", 0.01750, boost::gregorian::date(2019, Nov, 30));

  if (_cusip == "9128283G3")
    bond = Bond("9128283G3", CUSIP, "T", 0.01875, boost::gregorian::date(2020, Nov, 15));
  if (_cusip == "912828M80")
    bond = Bond("912828M80", CUSIP, "T", 0.02000, boost::gregorian::date(2022, Nov, 30));
  if (_cusip == "9128283J7")
    bond = Bond("9128283J7", CUSIP, "T", 0.02125, boost::gregorian::date(2024, Nov, 30));
  if (_cusip == "9128283F5")
    bond = Bond("9128283F5", CUSIP, "T", 0.02250, boost::gregorian::date(2027, Nov, 15));
  if (_cusip == "912810RZ3")
    bond = Bond("912810RZ3", CUSIP, "T", 0.02750, boost::gregorian::date(2047, Nov, 15));
  return bond;
}

#endif //CPP_FINAL_UTILITYFUNCTIONS_HPP
