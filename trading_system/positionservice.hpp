/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITIONSERVICE_HPP
#define POSITIONSERVICE_HPP

#include <string>
#include <map>
#include "soa.hpp"

#include "tradebookingservice.hpp"

using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template <typename T>
class Position {

 public:

  Position() = default;
  // ctor for a position
  Position(const T &_product);

  // Get the product
  const T &GetProduct() const;

  // Get the position quantity
  long GetPosition(const string &book);

  // update the position quantity of a particular book
  void updatePosition(const string &book, long newQuantity);

  // Get the aggregate position
  long GetAggregatePosition();

 private:
  T product;
  map<string, long> positions;

};

template <typename T>
Position<T>::Position(const T &_product) :
    product(_product) {
  positions.insert(std::pair<string, long>("TRSY1", 0));
  positions.insert(std::pair<string, long>("TRSY2", 0));
  positions.insert(std::pair<string, long>("TRSY3", 0));
}

template <typename T>
const T &Position<T>::GetProduct() const {
  return product;
}

template <typename T>
long Position<T>::GetPosition(const string &book) {
  return positions[book];
}

template <typename T>
void Position<T>::updatePosition(const string &book, long newQuantity) {
  long old_position = positions[book];
  old_position += newQuantity;
  positions[book] = old_position;
}

template <typename T>
long Position<T>::GetAggregatePosition() {
  // get the aggregate position across three books
  long agg_position = 0;

  for (auto &a: positions) {
    agg_position += a.second;
  }
  return agg_position;
}

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class PositionService : public Service<string, Position<T> > {

 public:

  // Add a trade to the service
  virtual void AddTrade(Trade<T> &_trade) = 0;

};

class BondPositionService : public PositionService<Bond> {
 private:
  std::map<string, Position<Bond>> positionMap;// map stores the position data
  std::vector<ServiceListener<Position<Bond>> *> listeners;// servicelisteners contains the trade data
  BondPositionService() {};// private ctor corresponding to the singleton pattern we use in this class
 public:
  //singleton pattern - create a global access to the object
  static BondPositionService *getInstance();
  BondPositionService(const BondPositionService &_source) = delete;//invalid the copy ctor
  void operator=(const BondPositionService &_source) = delete;// invalid the assignment operator
  void OnMessage(Position<Bond> &_position) override {}//Not a subscriber class; No implementation
  Position<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<Position<Bond>> *_listener) override;
  const vector<ServiceListener<Position<Bond>> *> &GetListeners() const override;
  void AddTrade(Trade<Bond> &_trade) override;// passing the trade data to the listeners

};

BondPositionService *BondPositionService::getInstance() {
  static BondPositionService instance;
  return &instance;
}

Position<Bond> &BondPositionService::GetData(string _key) {
  return positionMap[_key];
}

void BondPositionService::AddListener(ServiceListener<Position<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<Position<Bond>> *> &BondPositionService::GetListeners() const {
  return listeners;
}

void BondPositionService::AddTrade(Trade<Bond> &_trade) {
  // Receiving the trade data and passing the position data to the listener
  string id = _trade.GetProduct().GetProductId();
  string book = _trade.GetBook();
  long new_quantity = _trade.GetQuantity();
  // get the Position object related to this new trade
  if (positionMap.find(id) == positionMap.end()) {
    // if this product does not exist in the position map
    // create a new position object
    Position<Bond> new_position(_trade.GetProduct());
    new_position.updatePosition(book, new_quantity);
    // insert into the map
    positionMap.insert(std::pair<string, Position<Bond>>(id, new_position));
    //std::cout << "BondPositionService: AddTrade Method notifying the listeners...\n";
    for (auto &listener: listeners)
      listener->ProcessAdd(new_position);// passing the updated position data to the listeners
  } else {
    // if this product already exist
    // get the position object
    Position<Bond> &position = positionMap[id];
    // update the quantity
    position.updatePosition(book, new_quantity);
    //std::cout << "BondPositionService: AddTrade Method notifying the listeners...\n";
    for (auto &listener: listeners)
      listener->ProcessUpdate(position);// passing the updated position data to the listeners

  }

}

class BondPositionServiceListener : public ServiceListener<Trade<Bond>> {
 private:
  BondPositionService *BPServ;
  BondPositionServiceListener() {
    BPServ = BondPositionService::getInstance();
  }
 public:
  static BondPositionServiceListener *getInstance();
  void ProcessAdd(Trade<Bond> &_data) override;
  void ProcessRemove(Trade<Bond> &_data) override;
  void ProcessUpdate(Trade<Bond> &_data) override;
  BondPositionService *getService();
};

BondPositionServiceListener *BondPositionServiceListener::getInstance() {
  static BondPositionServiceListener instance;
  return &instance;
}

void BondPositionServiceListener::ProcessAdd(Trade<Bond> &_data) {
  BPServ->AddTrade(_data);
}

void BondPositionServiceListener::ProcessRemove(Trade<Bond> &_data) {

  //TBD
}

void BondPositionServiceListener::ProcessUpdate(Trade<Bond> &_data) {

  BPServ->AddTrade(_data);
}

BondPositionService *BondPositionServiceListener::getService() {
  return BPServ;
}
#endif
