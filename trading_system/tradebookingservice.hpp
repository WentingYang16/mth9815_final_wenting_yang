/**
 * tradebookingservice.hpp
 * Defines the data types and Service for trade booking.
 *
 * @author Breman Thuraisingham
 */
#ifndef TRADEBOOKINGSERVICE_HPP
#define TRADEBOOKINGSERVICE_HPP

#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include "soa.hpp"
#include "products.hpp"
#include "UtilityFunctions.hpp"
#include "executionservice.hpp"

// Trade sides
enum Side { BUY, SELL };

/**
 * Trade object with a price, side, and quantity on a particular book.
 * Type T is the product type.
 */
template <typename T>
class Trade {

 public:

  Trade() = default;
  // ctor for a trade
  Trade(const T &_product, string _tradeId, double _price, string _book, long _quantity, Side _side);

  // Get the product
  const T &GetProduct() const;

  // Get the trade ID
  const string &GetTradeId() const;

  // Get the mid price
  double GetPrice() const;

  // Get the book
  const string &GetBook() const;

  // Get the quantity
  long GetQuantity() const;

  // Get the side
  Side GetSide() const;

 private:
  T product;
  string tradeId;
  double price;
  string book;
  long quantity;
  Side side;

};

template <typename T>
Trade<T>::Trade(const T &_product, string _tradeId, double _price, string _book, long _quantity, Side _side) :
    product(_product) {
  tradeId = _tradeId;
  price = _price;
  book = _book;
  quantity = _quantity;
  side = _side;
}

template <typename T>
const T &Trade<T>::GetProduct() const {
  return product;
}

template <typename T>
const string &Trade<T>::GetTradeId() const {
  return tradeId;
}

template <typename T>
double Trade<T>::GetPrice() const {
  return price;
}

template <typename T>
const string &Trade<T>::GetBook() const {
  return book;
}

template <typename T>
long Trade<T>::GetQuantity() const {
  return quantity;
}

template <typename T>
Side Trade<T>::GetSide() const {
  return side;
}

//class BondTradeBookingServiceListener : public ServiceListener<Trade<Bond>>
//{
// public:
//  void ProcessAdd(const Trade<Bond> &data) override;
//  void ProcessRemove()
//};



/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on trade id.
 * Type T is the product type.
 */
template <typename T>
class TradeBookingService : public Service<string, Trade<T> > {

 public:

  // Book the trade
  virtual void BookTrade(Trade<T> &_trade) = 0;

};

class BondTradeBookingService : public TradeBookingService<Bond> {
 private:
  // keyed on trade id
  std::unordered_map<string, Trade<Bond>> tradeMap;// map stores the trade data
  std::vector<ServiceListener<Trade<Bond>> *> listeners;// servicelisteners contains the trade data
  BondTradeBookingService() {};// private ctor corresponding to the singleton pattern we use in this class
 public:
  //singleton pattern - create a global access to the object
  static BondTradeBookingService *getInstance();
  BondTradeBookingService(const BondTradeBookingService &_source) = delete;//invalid the copy ctor
  void operator=(const BondTradeBookingService &_source) = delete;// invalid the assignment operator
  void OnMessage(Trade<Bond> &_trade) override;
  Trade<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<Trade<Bond>> *_listener) override;
  const vector<ServiceListener<Trade<Bond>> *> &GetListeners() const override;
  void BookTrade(Trade<Bond> &_trade) override;// passing the trade data to the listeners

};

BondTradeBookingService *BondTradeBookingService::getInstance() {
  static BondTradeBookingService instance;
  return &instance;
}

void BondTradeBookingService::OnMessage(Trade<Bond> &_trade) {
  // store the trade into the tradeMap
  std::string tradeId = _trade.GetTradeId();
  if (tradeMap.find(tradeId) == tradeMap.end()) {
    tradeMap.insert(std::pair<string, Trade<Bond>>(tradeId, _trade));
  } else {
    tradeMap[tradeId] = _trade;
  }
  BookTrade(_trade);
}

Trade<Bond> &BondTradeBookingService::GetData(string _key) {
  // key is the trade id
  return tradeMap[_key];
}

void BondTradeBookingService::AddListener(ServiceListener<Trade<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<Trade<Bond>> *> &BondTradeBookingService::GetListeners() const {
  return listeners;
}

void BondTradeBookingService::BookTrade(Trade<Bond> &_trade) {
  // Book the trade
  // Passing the trade data to the listener
  //std::cout << "BondTradeBookingService: BookTrade Method...\n";
  for (auto &listener: listeners)
    listener->ProcessAdd(_trade);
}

class BondTradeBookingConnector : public Connector<Trade<Bond>> {
 private:
  BondTradeBookingService *BTBServ;
  BondTradeBookingConnector() {
    BTBServ = BondTradeBookingService::getInstance();
  }// correspond to the singleton pattern

 public:
  static BondTradeBookingConnector *getInstance();// global access to the object of this class
  void read_txt();
  BondTradeBookingService *getService();
  void Publish(Trade<Bond> &_data) override {}//This is a subscribe-only connector
};

BondTradeBookingConnector *BondTradeBookingConnector::getInstance() {
  static BondTradeBookingConnector instance;
  return &instance;
}
void BondTradeBookingConnector::read_txt() {
  // read from the trades.txt
  // construct Trade object
  // call OnMessage method
  std::vector<std::string> rowArray;
  std::string line;
  try {
    std::ifstream inFile("trades.txt");
    if (!inFile) {
      std::cout << "Fail to open trades.txt!\n";
    }
    getline(inFile, line);// get the header of the file
    while (getline(inFile, line)) {
      Trim(line);
      if (line.size() > 0) {
        boost::split(rowArray, line, boost::is_any_of("-,"));
        string tradeId(rowArray[0]);
        string productId(rowArray[1]);
        BondIdType idType;
        if (rowArray[2] == "CUSIP")
          idType = BondIdType::CUSIP;

        Side side;
        if (rowArray[3] == "BUY") {
          side = Side::BUY;
        }
        if (rowArray[3] == "SELL") {
          side = Side::SELL;
        }

        double price = std::stod(rowArray[4]);
        long quantity = std::stol(rowArray[6]);

        string books(rowArray[7]);

        Bond bond = GenerateBond(productId);
        Trade<Bond> trade(bond, tradeId, price, books, quantity, side);

        //BondInquiryService* BIServ = BondInquiryService::getInstance();
        BTBServ->OnMessage(trade);

      }
    }
    std::cout << "DONE: read trades.txt...\n";
  }
  catch (std::exception &e) {
    std::cout << e.what() << "\n";
  }

}

BondTradeBookingService *BondTradeBookingConnector::getService() {
  return BTBServ;
}

class BondTradeBookingServiceListener : public ServiceListener<ExecutionOrder<Bond>> {
 private:
  BondTradeBookingService *BTBServ;
  BondTradeBookingServiceListener() {
    BTBServ = BondTradeBookingService::getInstance();
    shuffle = 1;
  }

  int shuffle;

 public:
  static BondTradeBookingServiceListener *getInstance();
  void ProcessAdd(ExecutionOrder<Bond> &_data) override;
  void ProcessRemove(ExecutionOrder<Bond> &_data) override {}
  void ProcessUpdate(ExecutionOrder<Bond> &_data) override {}
};

BondTradeBookingServiceListener *BondTradeBookingServiceListener::getInstance() {
  static BondTradeBookingServiceListener instance;
  return &instance;
}

void BondTradeBookingServiceListener::ProcessAdd(ExecutionOrder<Bond> &_data) {
  string cusip = _data.GetProduct().GetProductId();
  string orderid = _data.GetOrderId();
  double price = _data.GetPrice();
  long visibleQuantity = _data.GetVisibleQuantity();
  long hiddenQuantity = _data.GetHiddenQuantity();
  long quantity = visibleQuantity + hiddenQuantity;
  Side side;
  string book;

  if (_data.GetPricingSide() == PricingSide::OFFER)
    side = Side::BUY;
  if (_data.GetPricingSide() == PricingSide::BID)
    side = Side::SELL;

  if (shuffle % 3 == 0)
    book = "TRSY1";
  if (shuffle % 3 == 1)
    book = "TRSY2";
  if (shuffle % 3 == 2)
    book = "TRSY3";

  Trade<Bond> trade(_data.GetProduct(), orderid, price, book, quantity, side);
  BTBServ->BookTrade(trade);
  shuffle += 1;

}

#endif
