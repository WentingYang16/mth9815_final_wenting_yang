//
// Created by Wenting Yang on 12/13/17.
//

#ifndef INPUT_HPP
#define INPUT_HPP

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <exception>
#include "UtilityFunctions.hpp"

using namespace std;

std::vector<string> cusip = {"9128283H1", "9128283G3", "912828M80", "9128283J7", "9128283F5", "912810RZ3"};

void generate_trades() {
  std::vector<long> quantity = {10000000, 20000000, 30000000, 40000000, 50000000};
  std::vector<string> book = {"TRSY1", "TRSY2", "TRSY3"};

  ofstream out;
  out.open("trades.txt");
  try {
    if (!out)
      std::cout << "Unable to open the file trades.txt!\n";
    else {
      out << "TradeId,ProductId,ProductIdType,side,price,quantity,books\n";
      int cnt = 0;
      for (int i = 0; i != 6; i++) {

        for (int j = 0; j != 10; j++) {

          out << "T" + to_string(cnt + 1) << ",";
          out << cusip[i] << ",";
          out << "CUSIP" << ",";
          if (j % 2 == 0) {
            out << "BUY" << ",";
            out << to_fractional(99) << ",";
          }
          if (j % 2 == 1) {
            out << "SELL" << ",";
            out << to_fractional(100) << ",";
          }

          out << quantity[j % 5] << ",";
          out << book[cnt % 3] << "\n";

          cnt++;

        }

      }
      std::cout << "DONE: Generate trades.txt\n";
    }

  }

  catch (std::exception &e) {
    e.what();
  }
}

void generate_inquiries() {
  std::vector<long> quantity = {10000000, 20000000, 30000000, 40000000, 50000000};
  //std::vector<string> book = {"TRSY1","TRSY2","TRSY3"};

  ofstream out;
  out.open("inquiries.txt");
  try {
    if (!out)
      std::cout << "Unable to open the file inquiries.txt!\n";
    else {
      out << "InquiryId,ProductId,ProductIdType,side,price,quantity,state\n";
      int cnt = 0;
      for (int i = 0; i != 6; i++) {

        for (int j = 0; j != 10; j++) {

          out << "I" + to_string(cnt + 1) << ",";
          out << cusip[i] << ",";
          out << "CUSIP" << ",";
          if (j % 2 == 0) {
            out << "BUY" << ",";
            out << to_fractional(99) << ",";
          }
          if (j % 2 == 1) {
            out << "SELL" << ",";
            out << to_fractional(101) << ",";
          }

          out << quantity[j % 5] << ",";
          out << "RECEIVED" << "\n";

          cnt++;

        }

      }
      std::cout << "DONE: Generate inquiries.txt\n";
    }

  }

  catch (std::exception &e) {
    e.what();
  }
}

void generate_mkt_data() {
  std::vector<long> quantity = {10000000, 20000000, 30000000, 40000000, 50000000};
  std::vector<double> spread = {1 / 128.0, 2 / 128.0, 3 / 128.0, 4 / 128.0, 3 / 128.0, 2 / 128.0, 1 / 128.0};
  ofstream out;
  out.open("marketdata.txt");
  try {
    if (!out)
      std::cout << "Unable to open the file marketdata.txt!\n";
    else {
      out
          << "productId,bid1,offer1,quantity1,bid2,offer2,quantity2,bid3,offer3,quantity3,bid4,offer4,quantity4,bid5,offer5,quantity5\n";
      for (int i = 0; i != 6; i++) {
        double mid_price = 99.0;
        double top_spread = 0.0;

        //std::vector<double> bid(5,0.0);
        //std::vector<double> ask(5,0.0);
        for (int j = 0; j != 1000000; j++) {
          top_spread = spread[j % 7];
          double spread_now = 0.0;
          // output to file
          out << cusip[i] << ",";
          for (int k = 0; k != 5; k++) {
            // mid price oscillation
            mid_price = mid_price + 1 / 265.0;
            // reset mid price
            if (mid_price > 101.0) {
              mid_price = 99.0;
            }
            // generate spread

            spread_now = top_spread + k / 128.0;
            // generate bid price and offer price
            double bid = mid_price - spread_now / 2.0;
            double offer = mid_price + spread_now / 2.0;
            //std::cout << mid_price<<","<<spread_now<<";; ";
            out << to_fractional(bid) << ",";
            out << to_fractional(offer) << ",";
            out << quantity[k] << ",";

          }
          //std::cout << "\n";
          out << "\n";
        }
      }
      std::cout << "DONE: Generate marketdata.txt\n";
    }
  }
  catch (std::exception &e) {
    e.what();
  }
}

void generate_price() {
  std::vector<double> spread = {1 / 128.0, 3 / 256.0, 2 / 128.0};
  ofstream out;
  out.open("price.txt");
  try {
    if (!out)
      std::cout << "Unable to open the file price.txt!\n";
    else {
      out << "ProductId,MidPrice,BidOfferSpread\n";
      for (int i = 0; i != 6; i++) {
        double mid_price = 99.0;
        double spread_now = 0.0;

        for (int j = 0; j != 1000000; j++) {
          // generate spread
          spread_now = spread[j % 3];
          // output to file
          out << cusip[i] << ",";
          mid_price = mid_price + 1 / 256.0;
          // reset mid price
          if (mid_price > 101.0) {

            mid_price = 99.0;
          }
          out << to_fractional(mid_price) << ",";
          out << to_fractional(spread_now) << "\n";

        }

      }
      std::cout << "DONE: Generate price.txt\n";
    }
  }

  catch (std::exception &e) {
    e.what();
  }
}

#endif //CPP_FINAL_INPUT_HPP
