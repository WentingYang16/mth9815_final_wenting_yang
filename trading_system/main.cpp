#include <iostream>
#include <fstream>

#include <string>

#include "tradebookingservice.hpp"
#include "positionservice.hpp"
#include "riskservice.hpp"
#include"pricingservice.hpp"
#include "streamingservice.hpp"
#include "GUIService.hpp"
#include "historicaldataservice.hpp"
#include "UtilityFunctions.hpp"
#include "executionservice.hpp"
#include "inquiryservice.hpp"
#include "marketdataservice.hpp"
#include "input.hpp"
//#include <boost/date_time/gregorian/gregorian.hpp>


using namespace std;

int main() {

  //============  Generate Data ==================
  //generate market data
  generate_mkt_data();
  //generate price data
  generate_price();
  // generate inquiry data
  generate_inquiries();
  // generate trades data
  generate_trades();




  // ================ test trading system ===================
  //test BondInquiryService

  BondInquiryService *BIServ = BondInquiryService::getInstance();
  // set the connector
  BIServ->SetConnector(BondInquiryServiceConnector::getInstance());
  // add listeners
  // link the BondInquiryServiceListener to itself
  // link the BondHistoricalDataServiceListener to BondInquiryService
  BIServ->AddListener(BondInquiryServiceListener::getInstance());
  BIServ->AddListener(BondDataServListenerFromInquiry::getInstance());
  BondInquiryServiceConnector *BISConn = BondInquiryServiceConnector::getInstance();
  BondDataInquiryConnector *BDIConn = BondDataInquiryConnector::getInstance();
  // reset output file
  BDIConn->ResetOutputFile();
  // read from file "inquiries.txt"
  BISConn->read_txt();

  // test the
  // BondTradeBookingService; BondMarketDataService; BondAlgoExecutionService; BondExecutionService;
  // BondPositionService and BondRiskService



  BondMarketDataService *BMDServ = BondMarketDataService::getInstance();
  BondAlgoExecutionService *BAEServ = BondAlgoExecutionService::getInstance();
  BondExecutionService *BEServ = BondExecutionService::getInstance();
  BondTradeBookingService *BTBServ = BondTradeBookingService::getInstance();
  BondPositionService *BPServ = BondPositionService::getInstance();
  BondRiskService *BRServ = BondRiskService::getInstance();

  // add service listeners
  // link positionservice to tradebookingservice
  BTBServ->AddListener(BondPositionServiceListener::getInstance());
  // link historicaldataservice to positionservice
  // link riskservice to positionservice
  BPServ->AddListener(BondDataServListenerFromPosition::getInstance());
  BPServ->AddListener(BondRiskServiceListener::getInstance());
  // link historicaldataservice to riskservice
  BRServ->AddListener(BondDataServListenerFromRisk::getInstance());
  // link algoexecutionservice to marketdataservice
  BMDServ->AddListener(BondAlgoExecutionServiceListener::getInstance());
  // link executionservice to algoexecutionservice
  BAEServ->AddListener(BondExecutionServiceListener::getInstance());
  // link tradebookingservice to executionservice
  // link historicaldataservice to executionservice
  BEServ->AddListener(BondTradeBookingServiceListener::getInstance());
  BEServ->AddListener(BondDataServListenerFromExecution::getInstance());


  // reset output file
  BondDataRiskConnector *BDRConn = BondDataRiskConnector::getInstance();
  BondDataExecutionConnector *BDEConn = BondDataExecutionConnector::getInstance();
  BondDataPositionConnector *BDPConn = BondDataPositionConnector::getInstance();
  GUIServiceConnector *GUIConn = GUIServiceConnector::getInstance();
  BDPConn->ResetOutputFile();
  BDRConn->ResetOutputFile();
  BDEConn->ResetOutputFile();
  GUIConn->ResetOutputFile();
  // read file
  BondTradeBookingConnector *BTBConn = BondTradeBookingConnector::getInstance();
  BTBConn->read_txt();
  BondMarketDataConnector *BMDConn = BondMarketDataConnector::getInstance();
  BMDConn->read_txt();

  //test pricingservice; algostreamingservice; streamingservice

  // Reset output file
  BondDataStreamConnector *BDSConn = BondDataStreamConnector::getInstance();
  BDSConn->ResetOutputFile();

  BondPricingService *BPRICEServ = BondPricingService::getInstance();
  BondAlgoStreamingService *BASServ = BondAlgoStreamingService::getInstance();
  BondStreamingService *BSServ = BondStreamingService::getInstance();
  // add service listeners
  // link algostreamingservice to pricingservice
  // link guiservice to pricingservice
  BPRICEServ->AddListener(BondAlgoStreamingServiceListener::getInstance());
  GUIServiceListener *GUIServList = GUIServiceListener::getInstance();
  BPRICEServ->AddListener(GUIServList);
  // link streamingservice to algostreamingservice
  BASServ->AddListener(BondStreamingServiceListener::getInstance());
  // link historicaldataservice ot streamingservice
  BSServ->AddListener(BondDataServListenerFromStream::getInstance());


  // read txt and trigger the pricing-streaming system
  BondPricingServiceConnector *BPServConn = BondPricingServiceConnector::getInstance();
  BPServConn->read_txt();

  return 0;
}