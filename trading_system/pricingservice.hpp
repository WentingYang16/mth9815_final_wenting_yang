/**
 * pricingservice.hpp
 * Defines the data types and Service for internal prices.
 *
 * @author Breman Thuraisingham
 */
#ifndef PRICINGSERVICE_HPP
#define PRICINGSERVICE_HPP

#include <string>
#include "soa.hpp"
#include "products.hpp"
#include "UtilityFunctions.hpp"
#include <fstream>
#include <map>

using namespace std;
/**
 * A price object consisting of mid and bid/offer spread.
 * Type T is the product type.
 */
template <typename T>
class Price {

 public:

  // ctor for a price
  Price() = default;
  Price(const T &_product, double _mid, double _bidOfferSpread);

  // Get the product
  const T &GetProduct() const;

  // Get the mid price
  double GetMid() const;

  // Get the bid/offer spread around the mid
  double GetBidOfferSpread() const;

 private:
  T product;
  double mid;
  double bidOfferSpread;

};

template <typename T>
Price<T>::Price(const T &_product, double _mid, double _bidOfferSpread) :
    product(_product) {
  mid = _mid;
  bidOfferSpread = _bidOfferSpread;
}

template <typename T>
const T &Price<T>::GetProduct() const {
  return product;
}

template <typename T>
double Price<T>::GetMid() const {
  return mid;
}

template <typename T>
double Price<T>::GetBidOfferSpread() const {
  return bidOfferSpread;
}

/**
 * Pricing Service managing mid prices and bid/offers.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class PricingService : public Service<string, Price<T> > {
 public:
  virtual void SendPrice(Price<T> &_price)=0;
};

class BondPricingService : public PricingService<Bond> {
 private:
  std::map<string, Price<Bond>> priceMap;// map stores the price data
  std::vector<ServiceListener<Price<Bond>> *> listeners;// servicelisteners contains the trade data
  BondPricingService() {};// private ctor corresponding to the singleton pattern we use in this class
 public:
  //singleton pattern - create a global access to the object
  static BondPricingService *getInstance();
  BondPricingService(const BondPricingService &_source) = delete;//invalid the copy ctor
  void operator=(const BondPricingService &_source) = delete;// invalid the assignment operator
  void OnMessage(Price<Bond> &_price) override;
  Price<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<Price<Bond>> *_listener) override;
  const vector<ServiceListener<Price<Bond>> *> &GetListeners() const override;
  void SendPrice(Price<Bond> &_price) override;// passing the trade data to the listeners

};

BondPricingService *BondPricingService::getInstance() {
  static BondPricingService instance;
  return &instance;
}

void BondPricingService::OnMessage(Price<Bond> &_price) {
  //store the price into the priceMap
  std::string productId = _price.GetProduct().GetProductId();
  priceMap[productId] = _price;
  SendPrice(_price);

}

Price<Bond> &BondPricingService::GetData(string _key) {
  return priceMap[_key];
}

void BondPricingService::AddListener(ServiceListener<Price<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<Price<Bond>> *> &BondPricingService::GetListeners() const {
  return listeners;
}

void BondPricingService::SendPrice(Price<Bond> &_price) {
  //std::cout << "BondPricingService: SendPrice Method call its listeners\n";
  for (auto &listener: listeners)
    listener->ProcessAdd(_price);
}

class BondPricingServiceConnector : public Connector<Price<Bond>> {
 private:
  BondPricingService *BPServ;
  BondPricingServiceConnector() {
    BPServ = BondPricingService::getInstance();
  }
 public:
  static BondPricingServiceConnector *getInstance();
  void read_txt();
  BondPricingService *getService();
  void Publish(Price<Bond> &_data) override {}//This is a subscribe-only class
};

BondPricingServiceConnector *BondPricingServiceConnector::getInstance() {
  static BondPricingServiceConnector instance;
  return &instance;
}

void BondPricingServiceConnector::read_txt() {
  // read from the price.txt
  // construct Price object
  // call OnMessage method

  std::vector<std::string> rowArray;
  std::string line;
  try {
    std::ifstream inFile("price.txt");
    if (!inFile) {
      std::cout << "Fail to open price.txt!\n";
    }
    getline(inFile, line);// get the header of the file
    //int cnt_line = 0;
    while (getline(inFile, line)) {
      //cnt_line++;
      Trim(line);
      if (line.size() > 0) {

        boost::split(rowArray, line, boost::is_any_of(","));
        // get productId and generate bond object
        string productId(rowArray[0]);
        Bond bond = GenerateBond(productId);
        // get mid price and spread
        double mid_price = to_double(rowArray[1]);
        double spread = to_double(rowArray[2]);
        // construct price object
        Price<Bond> price(bond, mid_price, spread);
        // call onMessage
        BPServ->OnMessage(price);

      }
    }
    std::cout << "DONE: read price.txt...\n";
  }
  catch (std::exception &e) {
    e.what();
  }
}

BondPricingService *BondPricingServiceConnector::getService() {
  return BPServ;
}

#endif
