/**
 * executionservice.hpp
 * Defines the data types and Service for executions.
 *
 * @author Breman Thuraisingham
 */
#ifndef EXECUTIONSERVICE_HPP
#define EXECUTIONSERVICE_HPP

#include <string>
#include "soa.hpp"
#include "marketdataservice.hpp"

enum OrderType { FOK = 0, IOC = 1, MARKET = 2, LIMIT = 3, STOP = 4 };

enum Market { BROKERTEC = 0, ESPEED = 1, CME = 2 };

/**
 * An execution order that can be placed on an exchange.
 * Type T is the product type.
 */
template <typename T>
class ExecutionOrder {

 public:
  ExecutionOrder() = default;
  // ctor for an order
  ExecutionOrder(const T &_product,
                 PricingSide _side,
                 string _orderId,
                 OrderType _orderType,
                 double _price,
                 double _visibleQuantity,
                 double _hiddenQuantity,
                 string _parentOrderId,
                 bool _isChildOrder);

  // Get the product
  const T &GetProduct() const;

  // Get the order ID
  const string &GetOrderId() const;

  // Get the order type on this order
  OrderType GetOrderType() const;

  // Get the price on this order
  double GetPrice() const;

  // Get the visible quantity on this order
  long GetVisibleQuantity() const;

  // Get the hidden quantity
  long GetHiddenQuantity() const;

  // Get the parent order ID
  const string &GetParentOrderId() const;

  // Is child order?
  bool IsChildOrder() const;

  // get pricing side
  PricingSide GetPricingSide() const;

 private:
  T product;
  PricingSide side;
  string orderId;
  OrderType orderType;
  double price;
  double visibleQuantity;
  double hiddenQuantity;
  string parentOrderId;
  bool isChildOrder;

};

template <typename T>
ExecutionOrder<T>::ExecutionOrder(const T &_product,
                                  PricingSide _side,
                                  string _orderId,
                                  OrderType _orderType,
                                  double _price,
                                  double _visibleQuantity,
                                  double _hiddenQuantity,
                                  string _parentOrderId,
                                  bool _isChildOrder) :
    product(_product) {
  side = _side;
  orderId = _orderId;
  orderType = _orderType;
  price = _price;
  visibleQuantity = _visibleQuantity;
  hiddenQuantity = _hiddenQuantity;
  parentOrderId = _parentOrderId;
  isChildOrder = _isChildOrder;
}

template <typename T>
const T &ExecutionOrder<T>::GetProduct() const {
  return product;
}

template <typename T>
const string &ExecutionOrder<T>::GetOrderId() const {
  return orderId;
}

template <typename T>
OrderType ExecutionOrder<T>::GetOrderType() const {
  return orderType;
}

template <typename T>
double ExecutionOrder<T>::GetPrice() const {
  return price;
}

template <typename T>
long ExecutionOrder<T>::GetVisibleQuantity() const {
  return visibleQuantity;
}

template <typename T>
long ExecutionOrder<T>::GetHiddenQuantity() const {
  return hiddenQuantity;
}

template <typename T>
const string &ExecutionOrder<T>::GetParentOrderId() const {
  return parentOrderId;
}

template <typename T>
bool ExecutionOrder<T>::IsChildOrder() const {
  return isChildOrder;
}

template <typename T>
PricingSide ExecutionOrder<T>::GetPricingSide() const {
  return side;
}

template <typename T>
class AlgoExecution {
  // contains 1 data members:

  // ExecutionOrder object
 private:
  //T product;
  ExecutionOrder<T> EO;
 public:
  AlgoExecution() = default;
  AlgoExecution(const ExecutionOrder<T> &_EO);
  //const T &getProduct() const;
  const ExecutionOrder<T> &getExecutionOrder() const;
};

template <typename T>
AlgoExecution<T>::AlgoExecution(const ExecutionOrder<T> &_EO):
    EO(_EO) {}

template <typename T>
const ExecutionOrder<T> &AlgoExecution<T>::getExecutionOrder() const {
  return EO;
}

template <typename T>
class AlgoExecutionService : public Service<string, AlgoExecution<T>> {
 public:
  virtual void sendOrder(const OrderBook<Bond> &_order)=0;// aggress the top of the book; alternating the bid/offer order; send to the listener

};

class BondAlgoExecutionService : public AlgoExecutionService<Bond> {
 private:
  std::map<string, AlgoExecution<Bond>>
      algoExecutionMap;// map stores the AlgoExecution object, keyed on product identifier
  std::vector<ServiceListener<AlgoExecution<Bond>> *> listeners;// serviceListeners list
  BondAlgoExecutionService() {};//private ctor corresponding to the singleton pattern
  int cnt = 0;
 public:
  //singleton pattern - create a global access to the object
  static BondAlgoExecutionService *getInstance();
  BondAlgoExecutionService(const BondAlgoExecutionService &_source) = delete;//invalid the copy ctor
  void operator=(const BondAlgoExecutionService &_source) = delete;// invalid the assignment operator
  void OnMessage(AlgoExecution<Bond> &_algoexe) override {}
  AlgoExecution<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<AlgoExecution<Bond>> *_listener) override;
  const vector<ServiceListener<AlgoExecution<Bond>> *> &GetListeners() const override;
  void sendOrder(const OrderBook<Bond> &_order) override;

};

BondAlgoExecutionService *BondAlgoExecutionService::getInstance() {
  static BondAlgoExecutionService instance;
  return &instance;
}

AlgoExecution<Bond> &BondAlgoExecutionService::GetData(string _key) {
  return algoExecutionMap[_key];
}

void BondAlgoExecutionService::AddListener(ServiceListener<AlgoExecution<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<AlgoExecution<Bond>> *> &BondAlgoExecutionService::GetListeners() const {
  return listeners;
}

void BondAlgoExecutionService::sendOrder(const OrderBook<Bond> &_order) {
  // Do the Bond Execution Algorithm
  string productId = _order.GetProduct().GetProductId();
  Order bid = _order.GetBidStack()[0];
  Order offer = _order.GetOfferStack()[0];
  // check if the spread is at its tightest
  double bid_price = bid.GetPrice();
  double offer_price = offer.GetPrice();
  if (offer_price - bid_price == 1.0 / 128.0) {
    AlgoExecution<Bond> executionOrder = algoExecutionMap[productId];
    double price = 0.0;
    long quantity = 0;
    PricingSide side;
    if (executionOrder.getExecutionOrder().GetPricingSide() == PricingSide::OFFER) {
      side = PricingSide::BID;
      price = bid_price;
      quantity = bid.GetQuantity();
    }

    if (executionOrder.getExecutionOrder().GetPricingSide() == PricingSide::BID) {
      side = PricingSide::OFFER;
      price = offer_price;
      quantity = offer.GetQuantity();
    }

    string orderId;
    orderId = "O" + std::to_string(cnt);
    long visible = quantity / 3;
    long hidden = quantity - visible;
    OrderType ordertype = LIMIT;

    ExecutionOrder<Bond> EO
        (_order.GetProduct(), side, orderId, ordertype, price, visible, hidden, "NA", false);
    AlgoExecution<Bond> algoexe(EO);
    string productId = _order.GetProduct().GetProductId();
    algoExecutionMap[productId] = algoexe;
    //std::cout << "BondAlgoExecutionService: ExecuteOrder Method...\n";
    for (auto &listener:listeners)
      listener->ProcessAdd(algoexe);
    cnt++;
  }
}

class BondAlgoExecutionServiceListener : public ServiceListener<OrderBook<Bond>> {
 private:
  BondAlgoExecutionService *BAEServ;
  int shuffle;

  BondAlgoExecutionServiceListener() {
    BAEServ = BondAlgoExecutionService::getInstance();
    shuffle = 0;
  }

 public:
  static BondAlgoExecutionServiceListener *getInstance();
  void ProcessAdd(OrderBook<Bond> &_data) override;
  void ProcessRemove(OrderBook<Bond> &_data) override;
  void ProcessUpdate(OrderBook<Bond> &_data) override;
};

BondAlgoExecutionServiceListener *BondAlgoExecutionServiceListener::getInstance() {
  static BondAlgoExecutionServiceListener instance;
  return &instance;
}

void BondAlgoExecutionServiceListener::ProcessAdd(OrderBook<Bond> &_data) {

  BAEServ->sendOrder(_data);

}

void BondAlgoExecutionServiceListener::ProcessUpdate(OrderBook<Bond> &_data) {

  BAEServ->sendOrder(_data);

}

void BondAlgoExecutionServiceListener::ProcessRemove(OrderBook<Bond> &_data) {
  // No implementation
}

/**
 * Service for executing orders on an exchange.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class ExecutionService : public Service<string, ExecutionOrder<T> > {
 public:

  // Execute an order on a market
  virtual void ExecuteOrder(ExecutionOrder<T> &order, Market market) = 0;
};

class BondExecutionService : public ExecutionService<Bond> {
 private:
  std::map<string, ExecutionOrder<Bond>>
      executionOrderMap;// map stores the ExecutionOrder object, keyed on product identifier
  std::vector<ServiceListener<ExecutionOrder<Bond>> *> listeners;
  BondExecutionService() {};// private ctor corresponding to the singleton pattern
 public:
  static BondExecutionService *getInstance();
  BondExecutionService(const BondExecutionService &source) = delete;
  void operator=(const BondExecutionService &_source) = delete;// invalid the assignment operator
  void OnMessage(ExecutionOrder<Bond> &_executionOrder) override {}
  ExecutionOrder<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<ExecutionOrder<Bond>> *_listener) override;
  const vector<ServiceListener<ExecutionOrder<Bond>> *> &GetListeners() const override;
  void ExecuteOrder(ExecutionOrder<Bond> &order, Market market) override;
};

BondExecutionService *BondExecutionService::getInstance() {
  static BondExecutionService instance;
  return &instance;
}

ExecutionOrder<Bond> &BondExecutionService::GetData(string _key) {
  return executionOrderMap[_key];
}

void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<ExecutionOrder<Bond>> *> &BondExecutionService::GetListeners() const {
  return listeners;
}

void BondExecutionService::ExecuteOrder(ExecutionOrder<Bond> &order, Market market) {
  // send order to be executed
  // notify all the service listeners
  //std::cout << "BondAlgoExecutionService: ExecuteOrder Method...\n";
  for (auto &listener:listeners)
    listener->ProcessAdd(order);
}

class BondExecutionServiceListener : public ServiceListener<AlgoExecution<Bond>> {
 private:
  BondExecutionService *BEServ;

  BondExecutionServiceListener() {
    BEServ = BondExecutionService::getInstance();
  }

 public:
  static BondExecutionServiceListener *getInstance();
  void ProcessAdd(AlgoExecution<Bond> &_data) override;
  void ProcessRemove(AlgoExecution<Bond> &_data) override;
  void ProcessUpdate(AlgoExecution<Bond> &_data) override;
};

BondExecutionServiceListener *BondExecutionServiceListener::getInstance() {
  static BondExecutionServiceListener instance;
  return &instance;
}

void BondExecutionServiceListener::ProcessAdd(AlgoExecution<Bond> &_data) {
  ExecutionOrder<Bond> EO = _data.getExecutionOrder();
  BEServ->ExecuteOrder(EO, CME);
}

void BondExecutionServiceListener::ProcessUpdate(AlgoExecution<Bond> &_data) {
  ExecutionOrder<Bond> EO = _data.getExecutionOrder();
  BEServ->ExecuteOrder(EO, CME);
}

void BondExecutionServiceListener::ProcessRemove(AlgoExecution<Bond> &_data) {
  //TBD
}

#endif
