/**
 * marketdataservice.hpp
 * Defines the data types and Service for order book market data.
 *
 * @author Breman Thuraisingham
 */
#ifndef MARKETDATASERVICE_HPP
#define MARKETDATASERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include "soa.hpp"
#include "products.hpp"
#include "UtilityFunctions.hpp"

using namespace std;

// Side for market data
enum PricingSide { BID = 0, OFFER = 1 };

/**
 * A market data order with price, quantity, and side.
 */
class Order {

 public:
  Order() = default;
  // ctor for an order
  Order(double _price, long _quantity, PricingSide _side);

  // copy ctor for an order
  Order(const Order &source);

  // assignment operator overload
  void operator=(const Order &source);

  // Get the price on the order
  double GetPrice() const;

  // Get the quantity on the order
  long GetQuantity() const;

  // Get the side on the order
  PricingSide GetSide() const;

 private:
  double price;
  long quantity;
  PricingSide side;

};

Order::Order(double _price, long _quantity, PricingSide _side) {
  price = _price;
  quantity = _quantity;
  side = _side;
}

Order::Order(const Order &source) {
  price = source.price;
  quantity = source.quantity;
  side = source.side;
}

void Order::operator=(const Order &source) {
  price = source.price;
  quantity = source.quantity;
  side = source.side;
}

double Order::GetPrice() const {
  return price;
}

long Order::GetQuantity() const {
  return quantity;
}

PricingSide Order::GetSide() const {
  return side;
}

/**
 * Class representing a bid and offer order
 */
class BidOffer {

 public:

  BidOffer() = default;
  // ctor for bid/offer
  BidOffer(const Order &_bidOrder, const Order &_offerOrder);

  // Get the bid order
  const Order &GetBidOrder() const;

  // Get the offer order
  const Order &GetOfferOrder() const;

 private:
  Order bidOrder;
  Order offerOrder;

};

BidOffer::BidOffer(const Order &_bidOrder, const Order &_offerOrder) :
    bidOrder(_bidOrder), offerOrder(_offerOrder) {
}

const Order &BidOffer::GetBidOrder() const {
  return bidOrder;
}

const Order &BidOffer::GetOfferOrder() const {
  return offerOrder;
}

/**
 * Order book with a bid and offer stack.
 * Type T is the product type.
 */
template <typename T>
class OrderBook {

 public:

  OrderBook() = default;
  // ctor for the order book
  OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack);

  // Get the product
  const T &GetProduct() const;

  // Get the bid stack
  const vector<Order> &GetBidStack() const;

  // Get the offer stack
  const vector<Order> &GetOfferStack() const;

 private:
  T product;
  vector<Order> bidStack;
  vector<Order> offerStack;

};

template <typename T>
OrderBook<T>::OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack) :
    product(_product), bidStack(_bidStack), offerStack(_offerStack) {
}

template <typename T>
const T &OrderBook<T>::GetProduct() const {
  return product;
}

template <typename T>
const vector<Order> &OrderBook<T>::GetBidStack() const {
  return bidStack;
}

template <typename T>
const vector<Order> &OrderBook<T>::GetOfferStack() const {
  return offerStack;
}

/**
 * Market Data Service which distributes market data
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class MarketDataService : public Service<string, OrderBook<T> > {

 public:

  // Get the best bid/offer order
  virtual const BidOffer &GetBestBidOffer(const string &_productId) = 0;

  // Aggregate the order book
  virtual const OrderBook<T> &AggregateDepth(const string &_productId) = 0;

};

class BondMarketDataService : public MarketDataService<Bond> {
 private:
  std::map<string, OrderBook<Bond>> orderBookMap;
  std::vector<ServiceListener<OrderBook<Bond>> *> listeners;
  BondMarketDataService() {};// corresponding to the singleton pattern

 public:
  static BondMarketDataService *getInstance();
  BondMarketDataService(const BondMarketDataService &_source) = delete;
  void operator=(const BondMarketDataService &_source) = delete;
  void OnMessage(OrderBook<Bond> &_orderbook) override;
  OrderBook<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<OrderBook<Bond>> *_listener) override;
  const vector<ServiceListener<OrderBook<Bond>> *> &GetListeners() const override;
  const BidOffer &GetBestBidOffer(const string &_productId) override;
  const OrderBook<Bond> &AggregateDepth(const string &_productId) override;
};

BondMarketDataService *BondMarketDataService::getInstance() {
  static BondMarketDataService instance;
  return &instance;
}

void BondMarketDataService::OnMessage(OrderBook<Bond> &_orderbook) {
  std::string productId = _orderbook.GetProduct().GetProductId();
  orderBookMap[productId] = _orderbook;
  // notifying service listener
  for (auto &listener: listeners)
    listener->ProcessAdd(_orderbook);
}

OrderBook<Bond> &BondMarketDataService::GetData(string _key) {
  return orderBookMap[_key];
}

void BondMarketDataService::AddListener(ServiceListener<OrderBook<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<OrderBook<Bond>> *> &BondMarketDataService::GetListeners() const {
  return listeners;
}

const BidOffer &BondMarketDataService::GetBestBidOffer(const string &_productId) {
  const OrderBook<Bond> &OB = orderBookMap[_productId];
  const Order &bestBid = OB.GetBidStack()[0];
  const Order &bestOffer = OB.GetOfferStack()[0];
  return BidOffer(bestBid, bestOffer);
}

const OrderBook<Bond> &BondMarketDataService::AggregateDepth(const string &_productId) {
  return orderBookMap[_productId];
}

class BondMarketDataConnector : public Connector<OrderBook<Bond>> {
 private:
  BondMarketDataService *BMDServ;
  BondMarketDataConnector() {
    BMDServ = BondMarketDataService::getInstance();
  }
 public:
  static BondMarketDataConnector *getInstance();
  void read_txt();
  BondMarketDataService *getService();
  void Publish(OrderBook<Bond> &_data) override {}//This is a subscribe-only class

};

BondMarketDataConnector *BondMarketDataConnector::getInstance() {
  static BondMarketDataConnector instance;
  return &instance;
}

void BondMarketDataConnector::read_txt() {
  // read from the marketdate.txt
  // construct OrderBook object
  // call OnMessage method


  std::vector<std::string> rowArray;
  std::string line;
  try {
    std::ifstream inFile("marketdata.txt");
    if (!inFile) {
      std::cout << "Fail to open marketdata.txt!\n";
    }
    getline(inFile, line);// get the header of the file
    //int cnt_line = 0;
    while (getline(inFile, line)) {
      //cnt_line++;
      Trim(line);
      if (line.size() > 0) {
        std::vector<double> bid_price;
        std::vector<double> offer_price;
        std::vector<long> quantity;

        boost::split(rowArray, line, boost::is_any_of(","));
        string productId(rowArray[0]);
        for (int i = 0; i < 5; i++) {
          bid_price.push_back(to_double(rowArray[i * 3 + 1]));
          offer_price.push_back(to_double(rowArray[i * 3 + 2]));
          quantity.push_back(std::stol(rowArray[i * 3 + 3]));
        }

        Bond bond = GenerateBond(productId);
        std::vector<Order> bidstack;
        std::vector<Order> offerstack;
        for (int i = 0; i < 5; i++) {
          Order bidorder(bid_price[i], quantity[i], BID);
          Order offerorder(offer_price[i], quantity[i], OFFER);
          bidstack.push_back(bidorder);
          offerstack.push_back(offerorder);
        }
        OrderBook<Bond> orderbook(bond, bidstack, offerstack);

        BMDServ->OnMessage(orderbook);

      }
    }
    std::cout << "DONE: read marketdata.txt...\n";
  }
  catch (std::exception &e) {
    std::cout << e.what() << "\n";
  }

}

BondMarketDataService *BondMarketDataConnector::getService() {
  return BMDServ;
}

#endif
