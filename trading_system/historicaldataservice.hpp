/**
 * historicaldataservice.hpp
 * historicaldataservice.hpp
 *
 * @author Breman Thuraisingham
 * Defines the data types and Service for historical data.
 *
 * @author Breman Thuraisingham
 */
#ifndef HISTORICALDATASERVICE_HPP
#define HISTORICALDATASERVICE_HPP

#include<string>
#include<vector>
#include<unordered_map>
#include<fstream>
#include<exception>
#include<ctime>
#include<cstdio>
#include"soa.hpp"
#include"positionservice.hpp"
#include"riskservice.hpp"
#include"executionservice.hpp"
#include"streamingservice.hpp"
#include"inquiryservice.hpp"

using namespace std;
/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */
template <typename T>
class HistoricalDataService : Service<string, T> {

 public:

  // Persist data to a store
  virtual void PersistData(string _persistKey, T &_data) = 0;

};

class BondDataPositionConnector : public Connector<Position<Bond>> {
 private:
  //BondHistoricalDataPositionService* BDPServ;
  BondDataPositionConnector() {
    //BDPServ = BondHistoricalDataPositionService::getInstance();
  }

 public:
  static BondDataPositionConnector *getInstance();
  void Publish(Position<Bond> &_position) override;
  void ResetOutputFile();
};

BondDataPositionConnector *BondDataPositionConnector::getInstance() {
  static BondDataPositionConnector instance;
  return &instance;
}

void BondDataPositionConnector::ResetOutputFile() {
  ofstream out;
  out.open("positions.txt", std::ios_base::trunc);
  out << "TimeStamp,ProductId,TRSY1,TRSY2,TRSY3,Aggregate\n";
}

void BondDataPositionConnector::Publish(Position<Bond> &_position) {
  // publish the position of each book and aggregate position
  ofstream out;
  out.open("positions.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file allpositions.txt!\n";
    else {
      timeval curTime;
      gettimeofday(&curTime, NULL);
      int milli = curTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

      char currentTime[84] = "";
      sprintf(currentTime, "%s:%d", buffer, milli);

      out << currentTime << ",";
      out << _position.GetProduct().GetProductId() << ",";
      out << _position.GetPosition("TRSY1") << ",";
      out << _position.GetPosition("TRSY2") << ",";
      out << _position.GetPosition("TRSY3") << ",";
      out << _position.GetAggregatePosition() << "\n";
    }
  }
  catch (std::exception &e) {
    e.what();
  }

}

class BondHistoricalDataPositionService : public HistoricalDataService<Position<Bond>> {
 private:
  std::unordered_map<string, Position<Bond>> positionMap;
  BondDataPositionConnector *BDPConn;
  BondHistoricalDataPositionService() {
    BDPConn = BondDataPositionConnector::getInstance();
  }

 public:
  static BondHistoricalDataPositionService *getInstance();
  BondHistoricalDataPositionService(const BondHistoricalDataPositionService &_source) = delete;
  void operator=(const BondHistoricalDataPositionService &_source)= delete;
  void OnMessage(Position<Bond> &_position) override {}// No implementation
  Position<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<Position<Bond>> *_listener) override {}//No implementation
  const vector<ServiceListener<Position<Bond>> *> &GetListeners() const override {}// No implementation
  void PersistData(string _persistKey, Position<Bond> &_data) override;
  //void PublishData();// after all the position has been updated, publish the positions via the connector
};

BondHistoricalDataPositionService *BondHistoricalDataPositionService::getInstance() {
  static BondHistoricalDataPositionService instance;
  return &instance;
}

Position<Bond> &BondHistoricalDataPositionService::GetData(string _key) {
  return positionMap[_key];
}

void BondHistoricalDataPositionService::PersistData(string _persistKey, Position<Bond> &_data) {
  if (positionMap.find(_persistKey) == positionMap.end()) {
    positionMap.insert(std::pair<string, Position<Bond>>(_persistKey, _data));
  } else {
    positionMap[_persistKey] = _data;
  }
  BDPConn->Publish(_data);
}

//void BondHistoricalDataPositionService::PublishData(){
//  for(auto &i: positionMap)
//    BDPConn->Publish(i.second);
//}




class BondDataServListenerFromPosition : public ServiceListener<Position<Bond>> {
 private:
  BondHistoricalDataPositionService *BDPServ;
  BondDataServListenerFromPosition() {
    BDPServ = BondHistoricalDataPositionService::getInstance();
  }

 public:
  static BondDataServListenerFromPosition *getInstance();
  void ProcessAdd(Position<Bond> &_position) override;
  void ProcessUpdate(Position<Bond> &_position) override;
  void ProcessRemove(Position<Bond> &_position) override {}
};

BondDataServListenerFromPosition *BondDataServListenerFromPosition::getInstance() {
  static BondDataServListenerFromPosition instance;
  return &instance;
}

void BondDataServListenerFromPosition::ProcessAdd(Position<Bond> &_position) {
  BDPServ->PersistData(_position.GetProduct().GetProductId(), _position);
}

void BondDataServListenerFromPosition::ProcessUpdate(Position<Bond> &_position) {
  BDPServ->PersistData(_position.GetProduct().GetProductId(), _position);
}






///////////////////////////////////////

class BondDataRiskConnector : public Connector<PV01<Bond>> {
 private:
  //BondRiskService* BRServ;// a pointer from bond risk service in order to get the bucketed risk
  //BondHistoricalDataRiskService* BDRServ;
  BondDataRiskConnector() {
    //BDRServ = BondHistoricalDataRiskService::getInstance();
    //BRServ = BondRiskService::getInstance();
  }

 public:
  static BondDataRiskConnector *getInstance();
  void Publish(PV01<Bond> &_risk) override;
  void PublishBucket(PV01<BucketedSector<Bond>> &_risk);
  void ResetOutputFile();
};

BondDataRiskConnector *BondDataRiskConnector::getInstance() {
  static BondDataRiskConnector instance;
  return &instance;
}

void BondDataRiskConnector::ResetOutputFile() {
  ofstream out;
  out.open("risk.txt", std::ios_base::trunc);
  out << "TimeStamp,ProductId,PV01,Position\n";
}

void BondDataRiskConnector::Publish(PV01<Bond> &_risk) {
  ofstream out;
  out.open("risk.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file.\n";
    else {
      timeval curTime;
      gettimeofday(&curTime, NULL);
      int milli = curTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

      char currentTime[84] = "";
      sprintf(currentTime, "%s:%d", buffer, milli);

      out << currentTime << ",";
      string productId = _risk.GetProduct().GetProductId();
      out << productId << ",";
      out << _risk.GetPV01() << ",";
      out << _risk.GetQuantity() << "\n";

    }
  }
  catch (std::exception &e) {
    e.what();
  }

}

void BondDataRiskConnector::PublishBucket(PV01<BucketedSector<Bond>> &_risk) {
  ofstream out;
  out.open("risk.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file.\n";
    else {
      timeval curTime;
      gettimeofday(&curTime, NULL);
      int milli = curTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

      char currentTime[84] = "";
      sprintf(currentTime, "%s:%d", buffer, milli);

      out << currentTime << ",";
      string name = _risk.GetProduct().GetName();
      out << name << ",";
      out << _risk.GetPV01() << ",";
      out << _risk.GetQuantity() << "\n";

    }
  }
  catch (std::exception &e) {
    std::cout << e.what() << "\n";
  }

}

class BondHistoricalDataRiskService : public HistoricalDataService<PV01<Bond>> {
 private:
  std::unordered_map<string, PV01<Bond>> riskMap;
  std::unordered_map<string, PV01<BucketedSector<Bond>>> bucketRiskMap;
  BondDataRiskConnector *BDRConn;

  BondHistoricalDataRiskService() {
    BDRConn = BondDataRiskConnector::getInstance();

  }

 public:
  static BondHistoricalDataRiskService *getInstance();
  BondHistoricalDataRiskService(const BondHistoricalDataRiskService &_source) = delete;
  void operator=(const BondHistoricalDataRiskService &_source)= delete;
  void OnMessage(PV01<Bond> &_pv01) override {}// No implementation
  PV01<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<PV01<Bond>> *_listener) override {}//No implementation
  const vector<ServiceListener<PV01<Bond>> *> &GetListeners() const override {}// No implementation
  void PersistData(string _persistKey, PV01<Bond> &_data) override;
  void PersistBucketData(string _persistKey, PV01<BucketedSector<Bond>> &_data);
  //void PublishData();
};

BondHistoricalDataRiskService *BondHistoricalDataRiskService::getInstance() {
  static BondHistoricalDataRiskService instance;
  return &instance;
}

PV01<Bond> &BondHistoricalDataRiskService::GetData(string _key) {
  return riskMap[_key];
}

void BondHistoricalDataRiskService::PersistData(string _persistKey, PV01<Bond> &_data) {
  if (riskMap.find(_persistKey) == riskMap.end()) {
    riskMap.insert(std::pair<string, PV01<Bond>>(_persistKey, _data));
  } else {
    riskMap[_persistKey] = _data;
  }
  BDRConn->Publish(_data);
}

void BondHistoricalDataRiskService::PersistBucketData(string _persistKey, PV01<BucketedSector<Bond>> &_data) {
//  if(bucketRiskMap.find(_persistKey)==bucketRiskMap.end())
//  {
//    bucketRiskMap.insert(std::pair<string,PV01<BucketedSector<Bond>>>(_persistKey,_data));
//  }
//  else{
//    bucketRiskMap[_persistKey] = _data;
//  }
  BDRConn->PublishBucket(_data);
}


//void BondHistoricalDataRiskService::PublishData() {
//  for(auto &i:riskMap)
//    BDRConn->Publish(i.second);
//
//}


class BondDataServListenerFromRisk : public ServiceListener<PV01<Bond>> {
 private:
  BondHistoricalDataRiskService *BDRServ;
  BondRiskService *BRServ;
  BondDataServListenerFromRisk() {
    BDRServ = BondHistoricalDataRiskService::getInstance();
    BRServ = BondRiskService::getInstance();
  }

 public:
  static BondDataServListenerFromRisk *getInstance();
  void ProcessAdd(PV01<Bond> &_risk) override;
  void ProcessUpdate(PV01<Bond> &_risk) override;
  void ProcessRemove(PV01<Bond> &_risk) override {}
};

BondDataServListenerFromRisk *BondDataServListenerFromRisk::getInstance() {
  static BondDataServListenerFromRisk instance;
  return &instance;
}

void BondDataServListenerFromRisk::ProcessAdd(PV01<Bond> &_risk) {
  //std::cout<<"Risk data process to historical data service listener...\n";
  string productId = _risk.GetProduct().GetProductId();
  BDRServ->PersistData(productId, _risk);


  // generate the bucketsector risk object
  BucketedSector<Bond> sector = GenerateBucket(productId);
  PV01<BucketedSector<Bond>> BucketedRisk = BRServ->GetBucketedRisk(sector);
  BDRServ->PersistBucketData(sector.GetName(), BucketedRisk);

}

void BondDataServListenerFromRisk::ProcessUpdate(PV01<Bond> &_risk) {

}

///////////////////////////////////
//
class BondDataExecutionConnector : public Connector<ExecutionOrder<Bond>> {
 private:
  //BondHistoricalDataExecutionService* BDEServ;
  BondDataExecutionConnector() {
    //BDEServ = BondHistoricalDataExecutionService::getInstance();
  }

 public:
  static BondDataExecutionConnector *getInstance();
  void Publish(ExecutionOrder<Bond> &_execution) override;
  void ResetOutputFile();
};

BondDataExecutionConnector *BondDataExecutionConnector::getInstance() {
  static BondDataExecutionConnector instance;
  return &instance;
}

void BondDataExecutionConnector::ResetOutputFile() {
  ofstream out;
  out.open("execution.txt", std::ios_base::trunc);
  out << "TimeStamp,OrderId,ProductId,Price,Side,OrderType,VisibleQuantity,HiddenQuantity,ParentOrderId,IsChildOrder\n";
}

void BondDataExecutionConnector::Publish(ExecutionOrder<Bond> &_execution) {
  ofstream out;
  out.open("execution.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the execution.txt file.\n";
    else {
      timeval curTime;
      gettimeofday(&curTime, NULL);
      int milli = curTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

      char currentTime[84] = "";
      sprintf(currentTime, "%s:%d", buffer, milli);

      out << currentTime << ",";
      out << _execution.GetOrderId() << ",";
      out << _execution.GetProduct().GetProductId() << ",";
      out << to_fractional(_execution.GetPrice()) << ",";
      if (_execution.GetPricingSide() == PricingSide::OFFER)
        out << "OFFER" << ",";
      if (_execution.GetPricingSide() == PricingSide::BID)
        out << "BID" << ",";

      if (_execution.GetOrderType() == OrderType::FOK)
        out << "FOK" << ",";
      if (_execution.GetOrderType() == OrderType::IOC)
        out << "IOC" << ",";
      if (_execution.GetOrderType() == OrderType::MARKET)
        out << "MARKET" << ",";
      if (_execution.GetOrderType() == OrderType::LIMIT)
        out << "LIMIT" << ",";
      if (_execution.GetOrderType() == OrderType::STOP)
        out << "STOP" << ",";

      out << _execution.GetVisibleQuantity() << ",";
      out << _execution.GetHiddenQuantity() << ",";
      out << _execution.GetParentOrderId() << ",";
      out << std::boolalpha << _execution.IsChildOrder() << "\n";

    }
  }
  catch (std::exception &e) {
    e.what();
  }

}

class BondHistoricalDataExecutionService : public HistoricalDataService<ExecutionOrder<Bond>> {
 private:
  //std::unordered_map<string,ExecutionOrder<Bond>> executionMap;
  BondDataExecutionConnector *BDEConn;
  BondHistoricalDataExecutionService() {
    BDEConn = BondDataExecutionConnector::getInstance();
  }

 public:
  static BondHistoricalDataExecutionService *getInstance();
  BondHistoricalDataExecutionService(const BondHistoricalDataExecutionService &_source) = delete;
  void operator=(const BondHistoricalDataExecutionService &_source)= delete;
  void OnMessage(ExecutionOrder<Bond> &_execution) override {}// No implementation
  ExecutionOrder<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<ExecutionOrder<Bond>> *_listener) override {}//No implementation
  const vector<ServiceListener<ExecutionOrder<Bond>> *> &GetListeners() const override {}// No implementation
  void PersistData(string _persistKey, ExecutionOrder<Bond> &_data) override;
};

BondHistoricalDataExecutionService *BondHistoricalDataExecutionService::getInstance() {
  static BondHistoricalDataExecutionService instance;
  return &instance;
}

ExecutionOrder<Bond> &BondHistoricalDataExecutionService::GetData(string _key) {
  //return executionMap[_key];
}

void BondHistoricalDataExecutionService::PersistData(string _persistKey, ExecutionOrder<Bond> &_data) {
  BDEConn->Publish(_data);
}

class BondDataServListenerFromExecution : public ServiceListener<ExecutionOrder<Bond>> {
 private:
  BondHistoricalDataExecutionService *BDEServ;
  BondDataServListenerFromExecution() {
    BDEServ = BondHistoricalDataExecutionService::getInstance();
  }

 public:
  static BondDataServListenerFromExecution *getInstance();
  void ProcessAdd(ExecutionOrder<Bond> &_execution) override;
  void ProcessUpdate(ExecutionOrder<Bond> &_execution) override;
  void ProcessRemove(ExecutionOrder<Bond> &_execution) override {}
};

BondDataServListenerFromExecution *BondDataServListenerFromExecution::getInstance() {
  static BondDataServListenerFromExecution instance;
  return &instance;
}

void BondDataServListenerFromExecution::ProcessAdd(ExecutionOrder<Bond> &_execution) {
  BDEServ->PersistData(_execution.GetProduct().GetProductId(), _execution);
}

void BondDataServListenerFromExecution::ProcessUpdate(ExecutionOrder<Bond> &_execution) {
  BDEServ->PersistData(_execution.GetProduct().GetProductId(), _execution);
}


///////////////////////////////////////

class BondDataStreamConnector : public Connector<PriceStream<Bond>> {
 private:
  //BondHistoricalDataStreamService* BDSServ;
  BondDataStreamConnector() {
    //BDSServ = BondHistoricalDataStreamService::getInstance();
  }

 public:
  static BondDataStreamConnector *getInstance();
  void Publish(PriceStream<Bond> &_execution) override;
  void ResetOutputFile();
};

BondDataStreamConnector *BondDataStreamConnector::getInstance() {
  static BondDataStreamConnector instance;
  return &instance;
}

void BondDataStreamConnector::ResetOutputFile() {
  ofstream out;
  out.open("streaming.txt", std::ios_base::trunc);
  out
      << "TimeStamp,ProductId,Side,BidPrice,BidHiddenQuantity,BidVisibleQuantity,Side,OfferPrice,OfferHiddenQuantity,OfferVisibleQuantity\n";
}

void BondDataStreamConnector::Publish(PriceStream<Bond> &_stream) {
  // publish to txt
  ofstream out;
  out.open("streaming.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file streaming.txt!\n";
    else {
      // print time stamp in millisecond accuracy
      timeval currentTime;
      gettimeofday(&currentTime, NULL);
      int milli = currentTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&currentTime.tv_sec));

      char curTime[84] = "";
      sprintf(curTime, "%s:%d", buffer, milli);

      out << curTime << ",";
      // print the PriceStream object
      out << _stream.GetProduct().GetProductId() << ",";
      out << "BID" << ",";
      out << to_fractional(_stream.GetBidOrder().GetPrice()) << ",";
      out << _stream.GetBidOrder().GetHiddenQuantity() << ",";
      out << _stream.GetBidOrder().GetVisibleQuantity() << ",";

      out << "OFFER" << ",";
      out << to_fractional(_stream.GetOfferOrder().GetPrice()) << ",";
      out << _stream.GetOfferOrder().GetHiddenQuantity() << ",";
      out << _stream.GetOfferOrder().GetVisibleQuantity() << "\n";

    }
  }
  catch (std::exception &e) {
    e.what();
  }

}

class BondHistoricalDataStreamService : public HistoricalDataService<PriceStream<Bond>> {
 private:
  //std::unordered_map<string,PriceStream<Bond>> streamMap;
  BondDataStreamConnector *BDSConn;
  BondHistoricalDataStreamService() {
    BDSConn = BondDataStreamConnector::getInstance();
  }

 public:
  static BondHistoricalDataStreamService *getInstance();
  BondHistoricalDataStreamService(const BondHistoricalDataStreamService &_source) = delete;
  void operator=(const BondHistoricalDataStreamService &_source)= delete;
  void OnMessage(PriceStream<Bond> &_execution) override {}// No implementation
  PriceStream<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<PriceStream<Bond>> *_listener) override {}//No implementation
  const vector<ServiceListener<PriceStream<Bond>> *> &GetListeners() const override {}// No implementation
  void PersistData(string _persistKey, PriceStream<Bond> &_data) override;
};

BondHistoricalDataStreamService *BondHistoricalDataStreamService::getInstance() {
  static BondHistoricalDataStreamService instance;
  return &instance;
}

PriceStream<Bond> &BondHistoricalDataStreamService::GetData(string _key) {
  //return streamMap[_key];
}

void BondHistoricalDataStreamService::PersistData(string _persistKey, PriceStream<Bond> &_data) {
  BDSConn->Publish(_data);
}

class BondDataServListenerFromStream : public ServiceListener<PriceStream<Bond>> {
 private:
  BondHistoricalDataStreamService *BDSServ;
  BondDataServListenerFromStream() {
    BDSServ = BondHistoricalDataStreamService::getInstance();
  }

 public:
  static BondDataServListenerFromStream *getInstance();
  void ProcessAdd(PriceStream<Bond> &_stream) override;
  void ProcessUpdate(PriceStream<Bond> &_stream) override;
  void ProcessRemove(PriceStream<Bond> &_stream) override {}
};

BondDataServListenerFromStream *BondDataServListenerFromStream::getInstance() {
  static BondDataServListenerFromStream instance;
  return &instance;
}

void BondDataServListenerFromStream::ProcessAdd(PriceStream<Bond> &_stream) {
  BDSServ->PersistData(_stream.GetProduct().GetProductId(), _stream);
}

void BondDataServListenerFromStream::ProcessUpdate(PriceStream<Bond> &_stream) {
  BDSServ->PersistData(_stream.GetProduct().GetProductId(), _stream);
}


/////////////////////////////////

class BondDataInquiryConnector : public Connector<Inquiry<Bond>> {
 private:
  //BondHistoricalDataInquiryService* BDIServ;
  BondDataInquiryConnector() {
    //BDIServ = BondHistoricalDataInquiryService::getInstance();
  }

 public:
  static BondDataInquiryConnector *getInstance();
  void Publish(Inquiry<Bond> &_inquiry) override;
  void ResetOutputFile();

};

BondDataInquiryConnector *BondDataInquiryConnector::getInstance() {
  static BondDataInquiryConnector instance;
  return &instance;
}

void BondDataInquiryConnector::ResetOutputFile() {
  ofstream out;
  out.open("allinquiries.txt", std::ios_base::trunc);
  out << "TimeStamp,InquiryId,ProductId,Price,Quantity,Side,State\n";
}

void BondDataInquiryConnector::Publish(Inquiry<Bond> &_inquiry) {
  ofstream out;
  out.open("allinquiries.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file.\n";
    else {
      timeval curTime;
      gettimeofday(&curTime, NULL);
      int milli = curTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

      char currentTime[84] = "";
      sprintf(currentTime, "%s:%d", buffer, milli);

      out << currentTime << ",";
      out << _inquiry.GetInquiryId() << ",";
      out << _inquiry.GetProduct().GetProductId() << ",";
      out << to_fractional(_inquiry.GetPrice()) << ",";
      out << _inquiry.GetQuantity() << ",";
      if (_inquiry.GetSide() == Side::BUY)
        out << "BUY" << ",";
      else
        out << "SELL" << ",";
      if (_inquiry.GetState() == InquiryState::DONE)
        out << "DONE" << "\n";
      if (_inquiry.GetState() == InquiryState::QUOTED)
        out << "QUOTED" << "\n";
      if (_inquiry.GetState() == InquiryState::RECEIVED)
        out << "RECEIVED" << "\n";
      if (_inquiry.GetState() == InquiryState::CUSTOMER_REJECTED)
        out << "CUSTOMER_REJECTED" << "\n";
      if (_inquiry.GetState() == InquiryState::REJECTED)
        out << "REJECTED" << "\n";

    }
  }
  catch (std::exception &e) {
    e.what();
  }
}

class BondHistoricalDataInquiryService : public HistoricalDataService<Inquiry<Bond>> {
 private:
  //std::unordered_map<string,Inquiry<Bond>> inquiryMap;
  BondDataInquiryConnector *BDIConn;
  BondHistoricalDataInquiryService() {
    BDIConn = BondDataInquiryConnector::getInstance();
  }

 public:
  static BondHistoricalDataInquiryService *getInstance();
  BondHistoricalDataInquiryService(const BondHistoricalDataInquiryService &_source) = delete;
  void operator=(const BondHistoricalDataInquiryService &_source)= delete;
  void OnMessage(Inquiry<Bond> &_execution) override {}// No implementation
  Inquiry<Bond> &GetData(string _key) override {}//No implementation
  void AddListener(ServiceListener<Inquiry<Bond>> *_listener) override {}//No implementation
  const vector<ServiceListener<Inquiry<Bond>> *> &GetListeners() const override {}// No implementation
  void PersistData(string _persistKey, Inquiry<Bond> &_data) override;
};

BondHistoricalDataInquiryService *BondHistoricalDataInquiryService::getInstance() {
  static BondHistoricalDataInquiryService instance;
  return &instance;
}

void BondHistoricalDataInquiryService::PersistData(string _persistKey, Inquiry<Bond> &_data) {
  BDIConn->Publish(_data);
}

class BondDataServListenerFromInquiry : public ServiceListener<Inquiry<Bond>> {
 private:
  BondHistoricalDataInquiryService *BDIServ;
  BondDataServListenerFromInquiry() {
    BDIServ = BondHistoricalDataInquiryService::getInstance();
  }

 public:
  static BondDataServListenerFromInquiry *getInstance();
  void ProcessAdd(Inquiry<Bond> &_inquiry) override;
  void ProcessUpdate(Inquiry<Bond> &_inquiry) override {}// No implementation
  void ProcessRemove(Inquiry<Bond> &_inquiry) override {}// No implementation
};

BondDataServListenerFromInquiry *BondDataServListenerFromInquiry::getInstance() {
  static BondDataServListenerFromInquiry instance;
  return &instance;
}

void BondDataServListenerFromInquiry::ProcessAdd(Inquiry<Bond> &_inquiry) {
  BDIServ->PersistData(_inquiry.GetProduct().GetProductId(), _inquiry);
}

#endif
