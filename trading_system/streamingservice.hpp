/**
 * streamingservice.hpp
 * Defines the data types and Service for price streams.
 *
 * @author Breman Thuraisingham
 */
#ifndef STREAMINGSERVICE_HPP
#define STREAMINGSERVICE_HPP

#include "soa.hpp"
#include "marketdataservice.hpp"
#include "pricingservice.hpp"

/**
 * A price stream order with price and quantity (visible and hidden)
 */

//enum PricingSide{ BID=0, OFFER=1 };
class PriceStreamOrder {

 public:

  // ctor for an order
  PriceStreamOrder() = default;
  PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side);

  // The side on this order
  PricingSide GetSide() const;

  // Get the price on this order
  double GetPrice() const;

  // Get the visible quantity on this order
  long GetVisibleQuantity() const;

  // Get the hidden quantity on this order
  long GetHiddenQuantity() const;

 private:
  double price;
  long visibleQuantity;
  long hiddenQuantity;
  PricingSide side;

};

PriceStreamOrder::PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side) {
  price = _price;
  visibleQuantity = _visibleQuantity;
  hiddenQuantity = _hiddenQuantity;
  side = _side;
}

double PriceStreamOrder::GetPrice() const {
  return price;
}

long PriceStreamOrder::GetVisibleQuantity() const {
  return visibleQuantity;
}

long PriceStreamOrder::GetHiddenQuantity() const {
  return hiddenQuantity;
}

/**
 * Price Stream with a two-way market.
 * Type T is the product type.
 */
template <typename T>
class PriceStream {

 public:

  // ctor
  PriceStream() = default;
  PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder);

  // Get the product
  const T &GetProduct() const;

  // Get the bid order
  const PriceStreamOrder &GetBidOrder() const;

  // Get the offer order
  const PriceStreamOrder &GetOfferOrder() const;

 private:
  T product;
  PriceStreamOrder bidOrder;
  PriceStreamOrder offerOrder;

};

template <typename T>
PriceStream<T>::PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder) :
    product(_product), bidOrder(_bidOrder), offerOrder(_offerOrder) {
}

template <typename T>
const T &PriceStream<T>::GetProduct() const {
  return product;
}

template <typename T>
const PriceStreamOrder &PriceStream<T>::GetBidOrder() const {
  return bidOrder;
}

template <typename T>
const PriceStreamOrder &PriceStream<T>::GetOfferOrder() const {
  return offerOrder;
}

template <typename T>
class AlgoStream {
  // contains 1 data members:
  // PriceStream object
 private:

  PriceStream<T> PS;

 public:
  AlgoStream() = default;
  AlgoStream(const PriceStream<T> &_PS);
  //const T &GetProduct() const;
  const PriceStream<T> &GetPriceStream() const;
};

template <typename T>
AlgoStream<T>::AlgoStream(const PriceStream<T> &_PS): PS(_PS) {}

template <typename T>
const PriceStream<T> &AlgoStream<T>::GetPriceStream() const {
  return PS;
}

template <typename T>
class AlgoStreamingService : public Service<string, AlgoStream<T>> {
 public:
  virtual void ProcessPrice(Price<T> &_price)=0;

};

class BondAlgoStreamingService : public AlgoStreamingService<Bond> {
 private:
  std::map<string, AlgoStream<Bond>> algoStreamMap;// map stores the AlgoExecution object, keyed on product identifier
  std::vector<ServiceListener<AlgoStream<Bond>> *> listeners;// serviceListeners list
  BondAlgoStreamingService() {};//private ctor corresponding to the singleton pattern
  int shuffle = 0;

 public:
  //singleton pattern - create a global access to the object
  static BondAlgoStreamingService *getInstance();
  BondAlgoStreamingService(const BondAlgoStreamingService &_source) = delete;//invalid the copy ctor
  void operator=(const BondAlgoStreamingService &_source) = delete;// invalid the assignment operator
  void OnMessage(AlgoStream<Bond> &_algostream) override {}// No implementation
  AlgoStream<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<AlgoStream<Bond>> *_listener) override;
  const vector<ServiceListener<AlgoStream<Bond>> *> &GetListeners() const override;
  void ProcessPrice(Price<Bond> &_price) override;
};

BondAlgoStreamingService *BondAlgoStreamingService::getInstance() {
  static BondAlgoStreamingService instance;
  return &instance;
}

AlgoStream<Bond> &BondAlgoStreamingService::GetData(string _key) {
  return algoStreamMap[_key];
}

void BondAlgoStreamingService::AddListener(ServiceListener<AlgoStream<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<AlgoStream<Bond>> *> &BondAlgoStreamingService::GetListeners() const {
  return listeners;
}

void BondAlgoStreamingService::ProcessPrice(Price<Bond> &_price) {
  double mid_price = _price.GetMid();
  double spread = _price.GetBidOfferSpread();
  double bid_price = mid_price - spread;
  double offer_price = mid_price + spread;
  double visible_quantity = 0.0;

  if (shuffle % 2 == 0) {
    visible_quantity = 1000000.0;
  }

  if (shuffle % 2 == 1) {
    visible_quantity = 2000000.0;
  }

  double hidden_quantity = 2 * visible_quantity;

  PriceStreamOrder bidOrder(bid_price, visible_quantity, hidden_quantity, PricingSide::BID);
  PriceStreamOrder offerOrder(offer_price, visible_quantity, hidden_quantity, PricingSide::OFFER);

  PriceStream<Bond> PS(_price.GetProduct(), bidOrder, offerOrder);
  AlgoStream<Bond> AS(PS);
  //std::cout << "BondAlgoStreamingService: SendPrice Method call its listeners\n";
  for (auto &listerner:listeners)
    listerner->ProcessAdd(AS);
}

class BondAlgoStreamingServiceListener : public ServiceListener<Price<Bond>> {
 private:
  BondAlgoStreamingService *BASServ;

  BondAlgoStreamingServiceListener() {
    BASServ = BondAlgoStreamingService::getInstance();
  }

 public:
  static BondAlgoStreamingServiceListener *getInstance();
  void ProcessAdd(Price<Bond> &_data) override;
  void ProcessRemove(Price<Bond> &_data) override {}
  void ProcessUpdate(Price<Bond> &_data) override;
};

BondAlgoStreamingServiceListener *BondAlgoStreamingServiceListener::getInstance() {
  static BondAlgoStreamingServiceListener instance;
  return &instance;
}

void BondAlgoStreamingServiceListener::ProcessAdd(Price<Bond> &_data) {
  BASServ->ProcessPrice(_data);
}

void BondAlgoStreamingServiceListener::ProcessUpdate(Price<Bond> &_data) {
  BASServ->ProcessPrice(_data);
}
/**
 * Streaming service to publish two-way prices.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template <typename T>
class StreamingService : public Service<string, PriceStream<T> > {

 public:

  // Publish two-way prices
  virtual void PublishPrice(PriceStream<T> &_priceStream) = 0;

};

class BondStreamingService : public StreamingService<Bond> {
 private:
  std::map<string, PriceStream<Bond>> priceStreamMap;// map stores the PriceStream object, keyed on product identifier
  std::vector<ServiceListener<PriceStream<Bond>> *> listeners;// serviceListeners list
  BondStreamingService() {};//private ctor corresponding to the singleton pattern

 public:
  //singleton pattern - create a global access to the object
  static BondStreamingService *getInstance();
  BondStreamingService(const BondStreamingService &_source) = delete;//invalid the copy ctor
  void operator=(const BondStreamingService &_source) = delete;// invalid the assignment operator
  void OnMessage(PriceStream<Bond> &_pricestream) override;
  PriceStream<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<PriceStream<Bond>> *_listener) override;
  const vector<ServiceListener<PriceStream<Bond>> *> &GetListeners() const override;
  void PublishPrice(PriceStream<Bond> &_priceStream) override;
};

BondStreamingService *BondStreamingService::getInstance() {
  static BondStreamingService instance;
  return &instance;
}

void BondStreamingService::OnMessage(PriceStream<Bond> &_pricestream) {
  string productId = _pricestream.GetProduct().GetProductId();
  priceStreamMap[productId] = _pricestream;
}

PriceStream<Bond> &BondStreamingService::GetData(string _key) {
  return priceStreamMap[_key];
}

void BondStreamingService::AddListener(ServiceListener<PriceStream<Bond>> *_listener) {
  listeners.push_back(_listener);
}

const vector<ServiceListener<PriceStream<Bond>> *> &BondStreamingService::GetListeners() const {
  return listeners;
}

void BondStreamingService::PublishPrice(PriceStream<Bond> &_priceStream) {
  // notify the service listeners
  //std::cout<< "BondStreamingService: PublishPrice Method call its listeners...\n";
  for (auto &listener:listeners)
    listener->ProcessAdd(_priceStream);
}

class BondStreamingServiceListener : public ServiceListener<AlgoStream<Bond>> {
 private:
  BondStreamingService *BSServ;
  BondStreamingServiceListener() {
    BSServ = BondStreamingService::getInstance();
    //shuffle = 1;
  }

  //int shuffle;

 public:
  static BondStreamingServiceListener *getInstance();
  void ProcessAdd(AlgoStream<Bond> &_data) override;
  void ProcessRemove(AlgoStream<Bond> &_data) override {}
  void ProcessUpdate(AlgoStream<Bond> &_data) override;
};

BondStreamingServiceListener *BondStreamingServiceListener::getInstance() {
  static BondStreamingServiceListener instance;
  return &instance;
}

void BondStreamingServiceListener::ProcessAdd(AlgoStream<Bond> &_data) {
  PriceStream<Bond> PS = _data.GetPriceStream();
  BSServ->PublishPrice(PS);
}

void BondStreamingServiceListener::ProcessUpdate(AlgoStream<Bond> &_data) {
  PriceStream<Bond> PS = _data.GetPriceStream();
  BSServ->PublishPrice(PS);
}

#endif
