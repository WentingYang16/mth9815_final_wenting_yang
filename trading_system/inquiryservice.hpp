/**
 * inquiryservice.hpp
 * Defines the data types and Service for customer inquiries.
 *
 * @author Breman Thuraisingham
 */
#ifndef INQUIRYSERVICE_HPP
#define INQUIRYSERVICE_HPP

#include "soa.hpp"
#include "products.hpp"
#include <string>
#include <unordered_map>
#include <fstream>
#include <boost/algorithm/string.hpp>
//#include <boost/date_time/gregorian/gregorian.hpp>
#include <exception>
#include "tradebookingservice.hpp"

using namespace std;
// Various inqyury states
enum InquiryState { RECEIVED, QUOTED, DONE, REJECTED, CUSTOMER_REJECTED };
//enum Side{BUY,SELL};
/**
 * Inquiry object modeling a customer inquiry from a client.
 * Type T is the product type.
 */
template <typename T>
class Inquiry {

 public:

  Inquiry() = default;
  // ctor for an inquiry
  Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state);

  // Get the inquiry ID
  const string &GetInquiryId() const;

  // Get the product
  const T &GetProduct() const;

  // Get the side on the inquiry
  Side GetSide() const;

  // Get the quantity that the client is inquiring for
  long GetQuantity() const;

  // Get the price that we have responded back with
  double GetPrice() const;

  // Get the current state on the inquiry
  InquiryState GetState() const;

 private:
  string inquiryId;
  T product;
  Side side;
  long quantity;
  double price;
  InquiryState state;

};

template <typename T>
Inquiry<T>::Inquiry(string _inquiryId,
                    const T &_product,
                    Side _side,
                    long _quantity,
                    double _price,
                    InquiryState _state) :
    product(_product) {
  inquiryId = _inquiryId;
  side = _side;
  quantity = _quantity;
  price = _price;
  state = _state;
}

template <typename T>
const string &Inquiry<T>::GetInquiryId() const {
  return inquiryId;
}

template <typename T>
const T &Inquiry<T>::GetProduct() const {
  return product;
}

template <typename T>
Side Inquiry<T>::GetSide() const {
  return side;
}

template <typename T>
long Inquiry<T>::GetQuantity() const {
  return quantity;
}

template <typename T>
double Inquiry<T>::GetPrice() const {
  return price;
}

template <typename T>
InquiryState Inquiry<T>::GetState() const {
  return state;
}

/**
 * Service for customer inquirry objects.
 * Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
 * Type T is the product type.
 */
template <typename T>
class InquiryService : public Service<string, Inquiry<T> > {

 public:

  // Send a quote back to the client
  virtual void SendQuote(const string &inquiryId, double price) = 0;

  // Reject an inquiry from the client
  virtual void RejectInquiry(const string &inquiryId) = 0;

};

class BondInquiryServiceConnector;
//BondInquiryServiceConnector * BondInquiryServiceConnector::getInstance();
class BondInquiryService : public InquiryService<Bond> {
 private:
  std::unordered_map<string, Inquiry<Bond>> inquiryMap;// map stores the inquiries data
  std::vector<ServiceListener<Inquiry<Bond>> *> listeners;// servicelisteners contains the trade data
  Connector<Inquiry<Bond>> *
      Conn;// connector (to avoid incomplete type error, we have to use a setter to set it when we test the whole program)
  BondInquiryService() {};// private ctor corresponding to the singleton pattern we use in this class
 public:
  //singleton pattern - create a global access to the object
  static BondInquiryService *getInstance();
  BondInquiryService(const BondInquiryService &_source) = delete;//invalid the copy ctor
  void operator=(const BondInquiryService &_source) = delete;// invalid the assignment operator
  void OnMessage(Inquiry<Bond> &_inquiry) override;
  Inquiry<Bond> &GetData(string _key) override;
  void AddListener(ServiceListener<Inquiry<Bond>> *_listener) override;
  const vector<ServiceListener<Inquiry<Bond>> *> &GetListeners() const override;
  void SetConnector(Connector<Inquiry<Bond>> *_Conn);// the setter function of the connector
  void SendQuote(const string &inquiryId, double price) override;
  void RejectInquiry(const string &inquiryId) override;

};

BondInquiryService *BondInquiryService::getInstance() {
  static BondInquiryService instance;
  return &instance;
}

Inquiry<Bond> &BondInquiryService::GetData(string _key) {
  return inquiryMap[_key];
}

void BondInquiryService::AddListener(ServiceListener<Inquiry<Bond>> *_listener) {
  listeners.push_back(_listener);
}

void BondInquiryService::SetConnector(Connector<Inquiry<Bond>> *_Conn) {
  Conn = _Conn;
}

const vector<ServiceListener<Inquiry<Bond>> *> &BondInquiryService::GetListeners() const {
  return listeners;
}

void BondInquiryService::OnMessage(Inquiry<Bond> &_inquiry) {

  string inquiryId = _inquiry.GetInquiryId();
  if (inquiryMap.find(inquiryId) == inquiryMap.end()) {
    inquiryMap.insert(std::pair<string, Inquiry<Bond>>(inquiryId, _inquiry));
  } else {
    inquiryMap[inquiryId] = _inquiry;
  }

  if (_inquiry.GetState() == RECEIVED) {
    for (auto &listener:listeners)
      listener->ProcessUpdate(_inquiry);
  }
  if (_inquiry.GetState() == QUOTED) {

    Inquiry<Bond> new_inquiry(_inquiry.GetInquiryId(),
                              _inquiry.GetProduct(),
                              _inquiry.GetSide(),
                              _inquiry.GetQuantity(),
                              _inquiry.GetPrice(),
                              DONE);
    for (auto &listener:listeners)
      listener->ProcessAdd(new_inquiry);
  }

  if (_inquiry.GetState() == REJECTED) {
    // reject the quote
    RejectInquiry(inquiryId);
  }

}

void BondInquiryService::SendQuote(const string &inquiryId, double price) {
// update the existing inquiry data using the new price
  Inquiry<Bond> inquiry = inquiryMap[inquiryId];

  Inquiry<Bond> new_inquiry
      (inquiry.GetInquiryId(), inquiry.GetProduct(), inquiry.GetSide(), inquiry.GetQuantity(), price, RECEIVED);


  // send this quote to the connector
  Conn->Publish(new_inquiry);
}

void BondInquiryService::RejectInquiry(const string &inquiryId) {
  // send the rejected inquiry to the historical data service
  Inquiry<Bond> inquiry = inquiryMap[inquiryId];
  for (auto &listener:listeners)
    listener->ProcessAdd(inquiry);
}

class BondInquiryServiceListener : public ServiceListener<Inquiry<Bond>> {
 private:
  BondInquiryService *BIServ;
  //BondInquiryServiceConnector* BIServConn;
  BondInquiryServiceListener() {
    BIServ = BondInquiryService::getInstance();
    //BIServConn = BondInquiryServiceConnector::getInstance();
  }

 public:
  static BondInquiryServiceListener *getInstance();
  void ProcessAdd(Inquiry<Bond> &_data) override;
  void ProcessRemove(Inquiry<Bond> &_data) override;
  void ProcessUpdate(Inquiry<Bond> &_data) override;
};

BondInquiryServiceListener *BondInquiryServiceListener::getInstance() {
  static BondInquiryServiceListener instance;
  return &instance;
}

void BondInquiryServiceListener::ProcessAdd(Inquiry<Bond> &_data) {}

void BondInquiryServiceListener::ProcessRemove(Inquiry<Bond> &_data) {}

void BondInquiryServiceListener::ProcessUpdate(Inquiry<Bond> &_data) {
  if (_data.GetState() == RECEIVED) {
    // send quote with RECEIVED state
    BIServ->SendQuote(_data.GetInquiryId(), 100);
  }

}

class BondInquiryServiceConnector : public Connector<Inquiry<Bond>> {
 private:
  BondInquiryService *BIServ;
  BondInquiryServiceConnector() {
    BIServ = BondInquiryService::getInstance();
  }// correspond to the singleton pattern

 public:
  static BondInquiryServiceConnector *getInstance();// global access to the object of this class
  void read_txt();

  void Publish(Inquiry<Bond> &_data) override;//This is a subscribe-only connector
};

BondInquiryServiceConnector *BondInquiryServiceConnector::getInstance() {
  static BondInquiryServiceConnector instance;
  return &instance;
}

void BondInquiryServiceConnector::read_txt() {
  std::vector<std::string> rowArray;
  std::string line;
  try {
    std::ifstream inFile("inquiries.txt");
    if (!inFile) {
      std::cout << "Fail to open inquiries.txt!\n";
    }
    getline(inFile, line);// get the header of the file
    while (getline(inFile, line)) {
      Trim(line);
      if (line.size() > 0) {
        boost::split(rowArray, line, boost::is_any_of("-,"));
        string inquiryId(rowArray[0]);
        string productId(rowArray[1]);
        BondIdType idType;
        if (rowArray[2] == "CUSIP")
          idType = BondIdType::CUSIP;

        Side side;
        if (rowArray[3] == "BUY") {
          side = Side::BUY;
        }
        if (rowArray[3] == "SELL") {
          side = Side::SELL;
        }

        double price = std::stod(rowArray[4]);
        long quantity = std::stol(rowArray[6]);
        InquiryState state;
        if (rowArray[7] == "RECEIVED")
          state = InquiryState::RECEIVED;

        Bond bond = GenerateBond(productId);

        Inquiry<Bond> inquiry(inquiryId, bond, side, quantity, price, state);

        //BondInquiryService* BIServ = BondInquiryService::getInstance();
        BIServ->OnMessage(inquiry);

      }
    }
    std::cout << "DONE: read inquiries.txt...\n";
  }
  catch (std::exception &e) {
    e.what();
  }

}

void BondInquiryServiceConnector::Publish(Inquiry<Bond> &_data) {
  if (_data.GetState() == RECEIVED && _data.GetPrice() == 100) {
    // change the State to QUOTED
    // and send back to the service
    Inquiry<Bond> new_inquiry
        (_data.GetInquiryId(), _data.GetProduct(), _data.GetSide(), _data.GetQuantity(), _data.GetPrice(), QUOTED);
    BondInquiryService *BIServ = BondInquiryService::getInstance();
    BIServ->OnMessage(new_inquiry);
  }

}

#endif
