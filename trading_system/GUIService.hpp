//
// Created by Wenting Yang on 12/11/17.
//

#ifndef GUISERVICE_HPP
#define GUISERVICE_HPP

#include <vector>
#include <unordered_map>
#include "pricingservice.hpp"
#include"products.hpp"
#include"UtilityFunctions.hpp"
#include "soa.hpp"
#include <chrono>
#include <thread>

class GUIServiceConnector : public Connector<Price<Bond>> {
  // a publish only connector with Publish method
  // No subscribe method implemented
 private:
  //GUIService* GUIServ;
  GUIServiceConnector() {
    //GUIServ = GUIService::getInstance();
  }

 public:
  static GUIServiceConnector *getInstance();
  //GUIService* getService();
  void Publish(Price<Bond> &_data) override;
  void ResetOutputFile();
};

GUIServiceConnector *GUIServiceConnector::getInstance() {
  static GUIServiceConnector instance;
  return &instance;
}

void GUIServiceConnector::ResetOutputFile() {
  ofstream out;
  out.open("gui.txt", std::ios_base::trunc);
  out << "TimeStamp,ProductId,MidPrice,BidOfferSpread\n";
}

void GUIServiceConnector::Publish(Price<Bond> &_data) {
  // write to txt with time stamp

  ofstream out;
  out.open("gui.txt", std::ios_base::app);
  try {
    if (!out)
      std::cout << "Unable to open the file streaming.txt!\n";
    else {

      // print time stamp in millisecond accuracy
      timeval currentTime;
      gettimeofday(&currentTime, NULL);
      int milli = currentTime.tv_usec / 1000;

      char buffer[80];
      strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&currentTime.tv_sec));

      char curTime[84] = "";
      sprintf(curTime, "%s:%d", buffer, milli);

      out << curTime << ",";
      // print the PriceStream object
      out << _data.GetProduct().GetProductId() << ",";
      out << to_fractional(_data.GetMid()) << ",";
      out << to_fractional(_data.GetBidOfferSpread()) << "\n";

    }
  }
  catch (std::exception &e) {
    e.what();
  }

}

class GUIService : public Service<string, Price<Bond>> {
 private:
  std::map<string, Price<Bond>> GUIMap;
  std::chrono::time_point<std::chrono::steady_clock> prev_time;
  GUIService() {
    GUIServConn = GUIServiceConnector::getInstance();
    //prev_time = std::chrono::steady_clock::now();// begin to count the throttles
  };
  GUIServiceConnector *GUIServConn;
  int cnt = 0;
 public:
  static GUIService *getInstance();
  GUIService(const GUIService &_source) = delete;
  void operator=(const GUIService &_source) = delete;
  void OnMessage(Price<Bond> &_price) override {}// A publish only service type; No implement of OnMessage method
  Price<Bond> &GetData(string _key) override;// No implementation
  void AddListener(ServiceListener<Price<Bond>> *_listener) override {}
  const vector<ServiceListener<Price<Bond>> *> &GetListeners() const override {}
  void PublishPrice(Price<Bond> &_price);
  void triggerTimer();

};

GUIService *GUIService::getInstance() {
  static GUIService instance;
  return &instance;
}

Price<Bond> &GUIService::GetData(string _key) {
  return GUIMap[_key];
}

void GUIService::PublishPrice(Price<Bond> &_price) {

  cnt++;
  if (cnt < 101) {
    //std::cout<<"Publish price...\n";
    GUIServConn->Publish(_price);
  } else
    return;
}

class GUIServiceListener : public ServiceListener<Price<Bond>> {
 private:
  Price<Bond> resumed_price;// store the last price got from pricingservice
  std::chrono::time_point<std::chrono::steady_clock> prev_time;
  GUIService *GUIServ;
  GUIServiceListener() {
    //std::cout<< "GUIServiceListener create!\n";
    GUIServ = GUIService::getInstance();
    //prev_time = std::chrono::steady_clock::now();// begin to count the throttles
  }
  int cnt = 0;
 public:
  static GUIServiceListener *getInstance();
  void ProcessAdd(Price<Bond> &_price) override;
  void ProcessRemove(Price<Bond> &_price) override {}
  void ProcessUpdate(Price<Bond> &_price) override;
  void notify();//notify the GUIService at the throttle
};

GUIServiceListener *GUIServiceListener::getInstance() {
  static GUIServiceListener instance;
  return &instance;
}

void GUIServiceListener::ProcessAdd(Price<Bond> &_price) {
  //std::cout<<"In GUIServiceListener ProcessAdd...\n";
  cnt++;
  if (cnt == 1) {
    // start the timer
    prev_time = std::chrono::steady_clock::now();
    GUIServ->PublishPrice(_price);

  } else {
    auto time_now = std::chrono::steady_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(time_now - prev_time).count();
    //std::cout << diff<<","<<std::chrono::milliseconds(300).count()<<std::endl;
    if (diff == (std::chrono::milliseconds(300)).count()) {

      GUIServ->PublishPrice(_price);
      prev_time = time_now;

    }
    if (diff > (std::chrono::milliseconds(300)).count()) {
      //std::cout<< "greater publish";
      //prev_time = prev_time+std::chrono::milliseconds(300);

      GUIServ->PublishPrice(resumed_price);
      prev_time = prev_time + std::chrono::milliseconds(300);
    }

  }

  resumed_price = _price;
}

void GUIServiceListener::ProcessUpdate(Price<Bond> &_price) {
  GUIServ->PublishPrice(_price);
}

#endif //CPP_FINAL_GUISERVICE_HPP
