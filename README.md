# README #


## The Structure of my code

### Directories: 
* __Root directory:__ trading_system - all the code file and two directories (input_file and output_file)
* __input_file:__ contains all the input txt files generated when I run my program (If you run the program, it will generate new input files under root directory)
* __output_file:__ contains all the output txt files  when I run my program  (If you run the program, it will generate new output files under root directory)
* __ReadMe:__ contains ReadMe files

### Code files and important functional Classes

* __executionservice.hpp__ : AlgoExecutionService(abstract class), BondAlgoExecutionService, BondAlgoExecutionServiceListener, ExecutionService(abstract class), BondExecutionService, BondExecutionServiceListener 

* __GUIService.hpp__: GUIService, GUIServiceListener, GUIServiceConnector

* __historicaldataservice.hpp__: HistoricalDataService(abstract class), BondDataPositionConnector, BondHistoricalDataPositionService, BondDataServListenerFromPosition; BondDataRiskConnector, BondHistoricalDataRiskService, BondDataServListenerFromRisk; BondDataExecutionConnector, BondHistoricalDataExecutionService, BondDataServListenerFromExecution; BondDataStreamConnector, BondHistoricalDataStreamService, BondDataServListenerFromStream; BondDataInquiryConnector, BondHistoricalDataInquiryService, BondDataServListenerFromInquiry

* __input.hpp__: utility function to generate input files

* __inquiryservice.hpp__: InquiryService(abstract class), BondInquiryService, BondInquiryServiceConnector, BondInquiryServiceListener

* __main.cpp__: the test file for the whole program

* __marketdataservice.hpp__: MarketDataService(abstract class), BondMarketDataService, BondMarketDataConnector

* __positionservice.hpp__: PositionService(abstract class), BondPositionService, BondPositionServiceListener

* __products.hpp__: Product(abstract class), Bond

* __riskservice.hpp__: RiskService(abstract class), BondRiskService, BondRiskServiceListener

* __soa.hpp__: base class of this SOA system - Connector(abstract class), Service(abstract class), ServiceListener(abstract class)

* __streamingservice.hpp__: AlgoStreamingService(abstract class), BondAlgoStreamingService, BondAlgoStreamingServiceListener, StreamingService(abstract class), BondStreamingService, BondStreamingServiceListener

* __tradebookingservice.hpp__: TradeBookingService(abstract class), BondTradeBookingService, BondTradeBookingConnector, BondTradeBookingServiceListener

* __UtilityFunctions.cpp__: contains some utility functions like to_double, to_fractional, GenerateBond

## How to RUN the program
####__Please note that this program contains boost library. Although in the CMakeList.txt, I have already create a code block to search the boost library in your computer, in case there are link errors, please change that to your local boost directories!___


* __Using terminal or command line to go to the root directory - trading_system__


* __type "ccmake . " and press enter__


* __type "c" to configure it and then type "g" to generate it and exit__


* __type "make" in the terminal or command line to build the files__


* __type "./trading_system" to run the program (due to the huge amount of data, it may take a while to finish running)__


## Or see the input and output directly?

__ Go to input_file and output_file directories! __

## The Structure of my bond trading system

### 1. Singleton Pattern

__All the Service and ServiceListener and Connector are designed in singleton pattern to accomodate the consistency of this trading system__

### 2. Four Bond Trading Service Line

#### Line 1: BondTradeBookingService - BondPositionService - BondRiskService - BondHistoricalDataService (BondHistoricalDataPositionService and BondHistoricalDataRiskService)
First, we use the generate_trades() function in the input.hpp file to generate the trades.txt, which contains the data to feed this service line.


In this line, we first use BondTradeBookingServiceConnector to read_txt() and then call the OnMessage method to add data to the BondTradeBookingService;
Then, we call the BookTrade method to call the ServiceListeners(send data to BondPositionService) in the BondTradeBookingService;
Once the BondPositionServiceListener(pass data from BondTradeBookingService to BondPositionService) get the data, it calls the AddTrade method in the BondPositionService. AddTrade method update the map in the BondPositionService and then call its ServiceListeners. BondPositionService has two ServiceListeners - BondRiskServiceListener (pass data from BondPositionService to BondRiskService) and BondDataServListenerFromPosition (pass data from BondPositionService to BondHistoricalDataPositionService).
The BondRiskServiceListener will call the AddPosition method to update the map in the BondRiskService and then call its ServiceListeners - BondDataServListenerFromRisk (pass data from BondRiskService to BondHistoricalDataRiskService).
After the BondHistoricalDataPositionService and BondHistoricalDataRiskService get data from BondPositionService and BondRiskService respectively, it will call the PersistData method, which will call the Publish Method of its Connector to output the positions and risk data to positions.txt and risk.txt.

__It is worth noting that, in order to realise the function of "persist risk for each security as well as for the bucket sectors", I create another map(bucketRiskMap) as a data member under the BondRiskService and a utility function UpdateBucketedRisk to update the bucket sector risk when new data arrives. Also I create an extra utility function under the BondHistoricalDataRiskService - PersistBucketData which will read the data from GetBucketedRisk when new data arrives; and an extra utility function under the BondDataRiskConnector - PublishBucket, which will publish the bucketed risk to risk.txt.__

#### Line 2: BondMarketDataService - BondAlgoExecutionService - BondExecutionService - BondHistoricalDataExecutionService and Line1(simultaneously)
This line is a extension to the line1.

First, we use the generate_mkt_data() function in the input.hpp file to generate the marketdata.txt, which contains the data to feed this service line.

In this line, we first use BondMarketDataServiceConnector to read_txt() and then call the OnMessage method to add data to the BondMarketDataService;Then, we call the ServiceListeners to send data to BondAlgoExecutionService.


Once the BondAlgoExecutionServiceListener(pass data from BondMarketDataService to BondAlgoExecutionService) get the data, it calls the sendOrder method in the BondAlgoExecutionService. sendOrder method aggress the tightest spread from OrderBook and create new ExecutionOrder and then call its ServiceListeners - BondExecutionServiceListener (pass data from BondPositionService to BondRiskService) and BondDataServListenerFromPosition (pass data from BondAlgoExecutionService to BondExecutionService).


The BondDataServListenerFromPosition will call the ExecuteOrder method in the BondExecutionService and then call its ServiceListeners - BondDataServListenerFromExecution (pass data from BondExecutionService to BondHistoricalDataRiskService) and BondTradeBookingServiceListener ((pass data from BondExecutionService to BondTradeBookingService).


After the BondDataServListenerFromExecution gets data from BondExecutionService, it will call the PersistData method, which will call the Publish Method of its Connector to output the execution order data to execution.txt.
Meanwhile, after BondTradeBookingService get data from BondExecutionService, it will add order to its trade map and continue the process in Line1.

#### Line 3: BondPriceService - GUIService (End) / BondAlgoStreamingService - BondStreamingService - BondHistoricalDataService (BondHistoricalDataStreamService)

First, we use the generate_price() function in the input.hpp file to generate the price.txt, which contains the data to feed this service line.

In this line, we first use BondPricingServiceConnector to read_txt() and then call the OnMessage method to add data to the BondPriceService;
Then we call the SendPrice Method to call its ServiceListeners - GUIServiceListener (pass data from BondPricingService to GUIService) and BondAlgoStreamingServiceListener (pass data from BondPricingService to BondAlgoStreamingService).


__In GUIService, I create a timer as a data member to count the time interval between each data passing in to make sure that every 300ms it will publish one data to the gui.txt.__


Once BondAlgoStreamingService received the data, it will generate the bid and offer PriceStreamOrder from mid price and spread and then generate a PriceStream and pass it to BondStreamingService via its ServiceListener(BondStreamingServiceListener). Once BondStreamingService receives the price, it will use PublishPrice method to call its ServiceListener (BondDataServListenerFromStream) to pass the data to BondHistoricalDataStreamService.
After the BondHistoricalDataStreamService gets data from BondStreamingService, it will call the PersistData method, which will call the Publish Method of its Connector to output the PriceStream data to streaming.txt.


#### Line 4: BondInquiryService - BondHistoricalDataInquiryService

First, we use the generate_inquiries() function in the input.hpp file to generate the inquiries.txt, which contains the data to feed this service line.

In this line, we first use __BondInquiryServiceConnector (A Subscribe and Publish Connector!)__ to read_txt() and then call the OnMessage method to add data to the BondInquiryService;
Once the BondInquiryService received the data, it will call its ServiceListener - BondInquiryServiceListener to process the data and turn it into RECEIVED state and then call the SendQuote method. In the SendQuote method, the price of this quote is changed to 100 and then it will send the QUOTE to the Connector via Publish() method. The Connector should transition the inquiry to the QUOTED state and send it back to the BondInquiryService via the OnMessage method with the supplied price. Once the BondInquiryService received the QUOTED data, it will change the state to DONE (in OnMessage function) and then call its ServiceListener - BondDataServListenerFromInquiry to publish the inquiry data.


After the BondHistoricalDataInquiryService gets data from BondInquiryService, it will call the PersistData method, which will call the Publish Method of its Connector to output the Inquiry data to allinquiries.txt.


### Who do I talk to? ###

* Repo owner: Wenting Yang wentingyang16@gmail.com
